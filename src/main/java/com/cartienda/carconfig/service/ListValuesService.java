package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.ListValues;
import com.cartienda.carconfig.repository.ListValuesRepository;
import com.cartienda.carconfig.service.dto.ListValuesDTO;
import com.cartienda.carconfig.service.mapper.ListValuesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ListValues.
 */
@Service
@Transactional
public class ListValuesService {

    private final Logger log = LoggerFactory.getLogger(ListValuesService.class);

    private final ListValuesRepository listValuesRepository;

    private final ListValuesMapper listValuesMapper;

    public ListValuesService(ListValuesRepository listValuesRepository, ListValuesMapper listValuesMapper) {
        this.listValuesRepository = listValuesRepository;
        this.listValuesMapper = listValuesMapper;
    }

    /**
     * Save a listValues.
     *
     * @param listValuesDTO the entity to save
     * @return the persisted entity
     */
    public ListValuesDTO save(ListValuesDTO listValuesDTO) {
        log.debug("Request to save ListValues : {}", listValuesDTO);
        ListValues listValues = listValuesMapper.toEntity(listValuesDTO);
        listValues = listValuesRepository.save(listValues);
        return listValuesMapper.toDto(listValues);
    }

    /**
     *  Get all the listValues.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ListValuesDTO> findAll() {
        log.debug("Request to get all ListValues");
        return listValuesRepository.findAll().stream()
            .map(listValuesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one listValues by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ListValuesDTO findOne(Long id) {
        log.debug("Request to get ListValues : {}", id);
        ListValues listValues = listValuesRepository.findOne(id);
        return listValuesMapper.toDto(listValues);
    }

    /**
     *  Delete the  listValues by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ListValues : {}", id);
        listValuesRepository.delete(id);
    }
}
