package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CartypesFieldsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CartypesFields and its DTO CartypesFieldsDTO.
 */
@Mapper(componentModel = "spring", uses = {FieldsMapper.class, CarTypesMapper.class, })
public interface CartypesFieldsMapper extends EntityMapper <CartypesFieldsDTO, CartypesFields> {

    @Mapping(source = "fields.id", target = "fieldsId")

    @Mapping(source = "carTypes.id", target = "carTypesId")
    CartypesFieldsDTO toDto(CartypesFields cartypesFields); 
    @Mapping(target = "listValues", ignore = true)

    @Mapping(source = "fieldsId", target = "fields")

    @Mapping(source = "carTypesId", target = "carTypes")
    CartypesFields toEntity(CartypesFieldsDTO cartypesFieldsDTO); 
    default CartypesFields fromId(Long id) {
        if (id == null) {
            return null;
        }
        CartypesFields cartypesFields = new CartypesFields();
        cartypesFields.setId(id);
        return cartypesFields;
    }
}
