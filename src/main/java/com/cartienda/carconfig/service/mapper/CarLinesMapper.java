package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CarLinesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CarLines and its DTO CarLinesDTO.
 */
@Mapper(componentModel = "spring", uses = {CarbrandsCarTypesMapper.class, })
public interface CarLinesMapper extends EntityMapper <CarLinesDTO, CarLines> {

    @Mapping(source = "carbrandsCarTypes.id", target = "carbrandsCarTypesId")
    CarLinesDTO toDto(CarLines carLines); 
    @Mapping(target = "carVersions", ignore = true)
    @Mapping(target = "specificsCars", ignore = true)

    @Mapping(source = "carbrandsCarTypesId", target = "carbrandsCarTypes")
    CarLines toEntity(CarLinesDTO carLinesDTO); 
    default CarLines fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarLines carLines = new CarLines();
        carLines.setId(id);
        return carLines;
    }
}
