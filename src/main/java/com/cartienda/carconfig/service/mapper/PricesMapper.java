package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.PricesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Prices and its DTO PricesDTO.
 */
@Mapper(componentModel = "spring", uses = {SpecificsCarMapper.class, })
public interface PricesMapper extends EntityMapper <PricesDTO, Prices> {

    @Mapping(source = "specificsCar.id", target = "specificsCarId")
    PricesDTO toDto(Prices prices); 

    @Mapping(source = "specificsCarId", target = "specificsCar")
    Prices toEntity(PricesDTO pricesDTO); 
    default Prices fromId(Long id) {
        if (id == null) {
            return null;
        }
        Prices prices = new Prices();
        prices.setId(id);
        return prices;
    }
}
