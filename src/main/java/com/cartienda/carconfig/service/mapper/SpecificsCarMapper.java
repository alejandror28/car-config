package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.SpecificsCarDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SpecificsCar and its DTO SpecificsCarDTO.
 */
@Mapper(componentModel = "spring", uses = {CarTypesMapper.class, CarBrandsMapper.class, CarLinesMapper.class, CarVersionsMapper.class, YearsMapper.class, })
public interface SpecificsCarMapper extends EntityMapper <SpecificsCarDTO, SpecificsCar> {

    @Mapping(source = "carTypes.id", target = "carTypesId")

    @Mapping(source = "carBrands.id", target = "carBrandsId")

    @Mapping(source = "carLines.id", target = "carLinesId")

    @Mapping(source = "carVersions.id", target = "carVersionsId")

    @Mapping(source = "years.id", target = "yearsId")
    SpecificsCarDTO toDto(SpecificsCar specificsCar); 
    @Mapping(target = "prices", ignore = true)

    @Mapping(source = "carTypesId", target = "carTypes")

    @Mapping(source = "carBrandsId", target = "carBrands")

    @Mapping(source = "carLinesId", target = "carLines")

    @Mapping(source = "carVersionsId", target = "carVersions")

    @Mapping(source = "yearsId", target = "years")
    SpecificsCar toEntity(SpecificsCarDTO specificsCarDTO); 
    default SpecificsCar fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpecificsCar specificsCar = new SpecificsCar();
        specificsCar.setId(id);
        return specificsCar;
    }
}
