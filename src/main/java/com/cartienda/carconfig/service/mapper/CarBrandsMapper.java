package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CarBrandsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CarBrands and its DTO CarBrandsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CarBrandsMapper extends EntityMapper <CarBrandsDTO, CarBrands> {
    
    @Mapping(target = "carbrandsCarTypes", ignore = true)
    @Mapping(target = "specificsCars", ignore = true)
    CarBrands toEntity(CarBrandsDTO carBrandsDTO); 
    default CarBrands fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarBrands carBrands = new CarBrands();
        carBrands.setId(id);
        return carBrands;
    }
}
