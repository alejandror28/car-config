package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CarbrandsCarTypesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CarbrandsCarTypes and its DTO CarbrandsCarTypesDTO.
 */
@Mapper(componentModel = "spring", uses = {CarBrandsMapper.class, CarTypesMapper.class, })
public interface CarbrandsCarTypesMapper extends EntityMapper <CarbrandsCarTypesDTO, CarbrandsCarTypes> {

    @Mapping(source = "carBrands.id", target = "carBrandsId")

    @Mapping(source = "carTypes.id", target = "carTypesId")
    CarbrandsCarTypesDTO toDto(CarbrandsCarTypes carbrandsCarTypes); 
    @Mapping(target = "carLines", ignore = true)

    @Mapping(source = "carBrandsId", target = "carBrands")

    @Mapping(source = "carTypesId", target = "carTypes")
    CarbrandsCarTypes toEntity(CarbrandsCarTypesDTO carbrandsCarTypesDTO); 
    default CarbrandsCarTypes fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarbrandsCarTypes carbrandsCarTypes = new CarbrandsCarTypes();
        carbrandsCarTypes.setId(id);
        return carbrandsCarTypes;
    }
}
