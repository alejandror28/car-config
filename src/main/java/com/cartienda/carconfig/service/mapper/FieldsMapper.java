package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.FieldsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Fields and its DTO FieldsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FieldsMapper extends EntityMapper <FieldsDTO, Fields> {
    
    @Mapping(target = "carFields", ignore = true)
    Fields toEntity(FieldsDTO fieldsDTO); 
    default Fields fromId(Long id) {
        if (id == null) {
            return null;
        }
        Fields fields = new Fields();
        fields.setId(id);
        return fields;
    }
}
