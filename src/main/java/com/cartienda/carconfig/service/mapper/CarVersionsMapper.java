package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CarVersionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CarVersions and its DTO CarVersionsDTO.
 */
@Mapper(componentModel = "spring", uses = {CarLinesMapper.class, })
public interface CarVersionsMapper extends EntityMapper <CarVersionsDTO, CarVersions> {

    @Mapping(source = "carLines.id", target = "carLinesId")
    CarVersionsDTO toDto(CarVersions carVersions); 
    @Mapping(target = "specificsCars", ignore = true)

    @Mapping(source = "carLinesId", target = "carLines")
    CarVersions toEntity(CarVersionsDTO carVersionsDTO); 
    default CarVersions fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarVersions carVersions = new CarVersions();
        carVersions.setId(id);
        return carVersions;
    }
}
