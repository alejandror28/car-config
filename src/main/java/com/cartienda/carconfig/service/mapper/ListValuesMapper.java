package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.ListValuesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ListValues and its DTO ListValuesDTO.
 */
@Mapper(componentModel = "spring", uses = {CartypesFieldsMapper.class, })
public interface ListValuesMapper extends EntityMapper <ListValuesDTO, ListValues> {

    @Mapping(source = "carTypesFields.id", target = "carTypesFieldsId")
    ListValuesDTO toDto(ListValues listValues); 

    @Mapping(source = "carTypesFieldsId", target = "carTypesFields")
    ListValues toEntity(ListValuesDTO listValuesDTO); 
    default ListValues fromId(Long id) {
        if (id == null) {
            return null;
        }
        ListValues listValues = new ListValues();
        listValues.setId(id);
        return listValues;
    }
}
