package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.CarTypesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CarTypes and its DTO CarTypesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CarTypesMapper extends EntityMapper <CarTypesDTO, CarTypes> {
    
    @Mapping(target = "carFields", ignore = true)
    @Mapping(target = "carbrandsCarTypes", ignore = true)
    @Mapping(target = "specificsCars", ignore = true)
    CarTypes toEntity(CarTypesDTO carTypesDTO); 
    default CarTypes fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarTypes carTypes = new CarTypes();
        carTypes.setId(id);
        return carTypes;
    }
}
