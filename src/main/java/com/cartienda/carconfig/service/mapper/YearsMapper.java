package com.cartienda.carconfig.service.mapper;

import com.cartienda.carconfig.domain.*;
import com.cartienda.carconfig.service.dto.YearsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Years and its DTO YearsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface YearsMapper extends EntityMapper <YearsDTO, Years> {
    
    @Mapping(target = "specificsCars", ignore = true)
    Years toEntity(YearsDTO yearsDTO); 
    default Years fromId(Long id) {
        if (id == null) {
            return null;
        }
        Years years = new Years();
        years.setId(id);
        return years;
    }
}
