package com.cartienda.carconfig.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Prices entity.
 */
public class PricesDTO implements Serializable {

    private Long id;

    @NotNull
    private String datePrices;

    @NotNull
    private BigDecimal price;

    private Long specificsCarId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatePrices() {
        return datePrices;
    }

    public void setDatePrices(String datePrices) {
        this.datePrices = datePrices;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getSpecificsCarId() {
        return specificsCarId;
    }

    public void setSpecificsCarId(Long specificsCarId) {
        this.specificsCarId = specificsCarId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PricesDTO pricesDTO = (PricesDTO) o;
        if(pricesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pricesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PricesDTO{" +
            "id=" + getId() +
            ", datePrices='" + getDatePrices() + "'" +
            ", price='" + getPrice() + "'" +
            "}";
    }
}
