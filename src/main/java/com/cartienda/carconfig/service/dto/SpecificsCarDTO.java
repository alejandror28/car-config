package com.cartienda.carconfig.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SpecificsCar entity.
 */
public class SpecificsCarDTO implements Serializable {

    private Long id;

    private Long carTypesId;

    private Long carBrandsId;

    private Long carLinesId;

    private Long carVersionsId;

    private Long yearsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCarTypesId() {
        return carTypesId;
    }

    public void setCarTypesId(Long carTypesId) {
        this.carTypesId = carTypesId;
    }

    public Long getCarBrandsId() {
        return carBrandsId;
    }

    public void setCarBrandsId(Long carBrandsId) {
        this.carBrandsId = carBrandsId;
    }

    public Long getCarLinesId() {
        return carLinesId;
    }

    public void setCarLinesId(Long carLinesId) {
        this.carLinesId = carLinesId;
    }

    public Long getCarVersionsId() {
        return carVersionsId;
    }

    public void setCarVersionsId(Long carVersionsId) {
        this.carVersionsId = carVersionsId;
    }

    public Long getYearsId() {
        return yearsId;
    }

    public void setYearsId(Long yearsId) {
        this.yearsId = yearsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpecificsCarDTO specificsCarDTO = (SpecificsCarDTO) o;
        if(specificsCarDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), specificsCarDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpecificsCarDTO{" +
            "id=" + getId() +
            "}";
    }
}
