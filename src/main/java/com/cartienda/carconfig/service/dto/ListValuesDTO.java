package com.cartienda.carconfig.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ListValues entity.
 */
public class ListValuesDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 60)
    private String listType;

    @NotNull
    @Size(max = 60)
    private String value;

    private Long carTypesFieldsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getCarTypesFieldsId() {
        return carTypesFieldsId;
    }

    public void setCarTypesFieldsId(Long cartypesFieldsId) {
        this.carTypesFieldsId = cartypesFieldsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ListValuesDTO listValuesDTO = (ListValuesDTO) o;
        if(listValuesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), listValuesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ListValuesDTO{" +
            "id=" + getId() +
            ", listType='" + getListType() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
