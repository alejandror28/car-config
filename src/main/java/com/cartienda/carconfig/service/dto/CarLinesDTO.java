package com.cartienda.carconfig.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CarLines entity.
 */
public class CarLinesDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String description;

    private Long carbrandsCarTypesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCarbrandsCarTypesId() {
        return carbrandsCarTypesId;
    }

    public void setCarbrandsCarTypesId(Long carbrandsCarTypesId) {
        this.carbrandsCarTypesId = carbrandsCarTypesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CarLinesDTO carLinesDTO = (CarLinesDTO) o;
        if(carLinesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carLinesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarLinesDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
