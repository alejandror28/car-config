package com.cartienda.carconfig.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CarVersions entity.
 */
public class CarVersionsDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String description;

    private Long carLinesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCarLinesId() {
        return carLinesId;
    }

    public void setCarLinesId(Long carLinesId) {
        this.carLinesId = carLinesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CarVersionsDTO carVersionsDTO = (CarVersionsDTO) o;
        if(carVersionsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carVersionsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarVersionsDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
