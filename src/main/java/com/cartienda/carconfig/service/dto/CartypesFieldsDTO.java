package com.cartienda.carconfig.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CartypesFields entity.
 */
public class CartypesFieldsDTO implements Serializable {

    private Long id;

    private Long fieldsId;

    private Long carTypesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFieldsId() {
        return fieldsId;
    }

    public void setFieldsId(Long fieldsId) {
        this.fieldsId = fieldsId;
    }

    public Long getCarTypesId() {
        return carTypesId;
    }

    public void setCarTypesId(Long carTypesId) {
        this.carTypesId = carTypesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CartypesFieldsDTO cartypesFieldsDTO = (CartypesFieldsDTO) o;
        if(cartypesFieldsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cartypesFieldsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CartypesFieldsDTO{" +
            "id=" + getId() +
            "}";
    }
}
