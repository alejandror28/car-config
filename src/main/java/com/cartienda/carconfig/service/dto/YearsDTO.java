package com.cartienda.carconfig.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Years entity.
 */
public class YearsDTO implements Serializable {

    private Long id;

    @NotNull
    private String year;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        YearsDTO yearsDTO = (YearsDTO) o;
        if(yearsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), yearsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "YearsDTO{" +
            "id=" + getId() +
            ", year='" + getYear() + "'" +
            "}";
    }
}
