package com.cartienda.carconfig.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CarbrandsCarTypes entity.
 */
public class CarbrandsCarTypesDTO implements Serializable {

    private Long id;

    private Long carBrandsId;

    private Long carTypesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCarBrandsId() {
        return carBrandsId;
    }

    public void setCarBrandsId(Long carBrandsId) {
        this.carBrandsId = carBrandsId;
    }

    public Long getCarTypesId() {
        return carTypesId;
    }

    public void setCarTypesId(Long carTypesId) {
        this.carTypesId = carTypesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CarbrandsCarTypesDTO carbrandsCarTypesDTO = (CarbrandsCarTypesDTO) o;
        if(carbrandsCarTypesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carbrandsCarTypesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarbrandsCarTypesDTO{" +
            "id=" + getId() +
            "}";
    }
}
