package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CartypesFields;
import com.cartienda.carconfig.repository.CartypesFieldsRepository;
import com.cartienda.carconfig.service.dto.CartypesFieldsDTO;
import com.cartienda.carconfig.service.mapper.CartypesFieldsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CartypesFields.
 */
@Service
@Transactional
public class CartypesFieldsService {

    private final Logger log = LoggerFactory.getLogger(CartypesFieldsService.class);

    private final CartypesFieldsRepository cartypesFieldsRepository;

    private final CartypesFieldsMapper cartypesFieldsMapper;

    public CartypesFieldsService(CartypesFieldsRepository cartypesFieldsRepository, CartypesFieldsMapper cartypesFieldsMapper) {
        this.cartypesFieldsRepository = cartypesFieldsRepository;
        this.cartypesFieldsMapper = cartypesFieldsMapper;
    }

    /**
     * Save a cartypesFields.
     *
     * @param cartypesFieldsDTO the entity to save
     * @return the persisted entity
     */
    public CartypesFieldsDTO save(CartypesFieldsDTO cartypesFieldsDTO) {
        log.debug("Request to save CartypesFields : {}", cartypesFieldsDTO);
        CartypesFields cartypesFields = cartypesFieldsMapper.toEntity(cartypesFieldsDTO);
        cartypesFields = cartypesFieldsRepository.save(cartypesFields);
        return cartypesFieldsMapper.toDto(cartypesFields);
    }

    /**
     *  Get all the cartypesFields.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CartypesFieldsDTO> findAll() {
        log.debug("Request to get all CartypesFields");
        return cartypesFieldsRepository.findAll().stream()
            .map(cartypesFieldsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one cartypesFields by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CartypesFieldsDTO findOne(Long id) {
        log.debug("Request to get CartypesFields : {}", id);
        CartypesFields cartypesFields = cartypesFieldsRepository.findOne(id);
        return cartypesFieldsMapper.toDto(cartypesFields);
    }

    /**
     *  Delete the  cartypesFields by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CartypesFields : {}", id);
        cartypesFieldsRepository.delete(id);
    }
}
