package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CarVersions;
import com.cartienda.carconfig.repository.CarVersionsRepository;
import com.cartienda.carconfig.service.dto.CarVersionsDTO;
import com.cartienda.carconfig.service.mapper.CarVersionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CarVersions.
 */
@Service
@Transactional
public class CarVersionsService {

    private final Logger log = LoggerFactory.getLogger(CarVersionsService.class);

    private final CarVersionsRepository carVersionsRepository;

    private final CarVersionsMapper carVersionsMapper;

    public CarVersionsService(CarVersionsRepository carVersionsRepository, CarVersionsMapper carVersionsMapper) {
        this.carVersionsRepository = carVersionsRepository;
        this.carVersionsMapper = carVersionsMapper;
    }

    /**
     * Save a carVersions.
     *
     * @param carVersionsDTO the entity to save
     * @return the persisted entity
     */
    public CarVersionsDTO save(CarVersionsDTO carVersionsDTO) {
        log.debug("Request to save CarVersions : {}", carVersionsDTO);
        CarVersions carVersions = carVersionsMapper.toEntity(carVersionsDTO);
        carVersions = carVersionsRepository.save(carVersions);
        return carVersionsMapper.toDto(carVersions);
    }

    /**
     *  Get all the carVersions.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CarVersionsDTO> findAll() {
        log.debug("Request to get all CarVersions");
        return carVersionsRepository.findAll().stream()
            .map(carVersionsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one carVersions by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CarVersionsDTO findOne(Long id) {
        log.debug("Request to get CarVersions : {}", id);
        CarVersions carVersions = carVersionsRepository.findOne(id);
        return carVersionsMapper.toDto(carVersions);
    }

    /**
     *  Delete the  carVersions by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarVersions : {}", id);
        carVersionsRepository.delete(id);
    }
}
