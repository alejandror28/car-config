package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.Years;
import com.cartienda.carconfig.repository.YearsRepository;
import com.cartienda.carconfig.service.dto.YearsDTO;
import com.cartienda.carconfig.service.mapper.YearsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Years.
 */
@Service
@Transactional
public class YearsService {

    private final Logger log = LoggerFactory.getLogger(YearsService.class);

    private final YearsRepository yearsRepository;

    private final YearsMapper yearsMapper;

    public YearsService(YearsRepository yearsRepository, YearsMapper yearsMapper) {
        this.yearsRepository = yearsRepository;
        this.yearsMapper = yearsMapper;
    }

    /**
     * Save a years.
     *
     * @param yearsDTO the entity to save
     * @return the persisted entity
     */
    public YearsDTO save(YearsDTO yearsDTO) {
        log.debug("Request to save Years : {}", yearsDTO);
        Years years = yearsMapper.toEntity(yearsDTO);
        years = yearsRepository.save(years);
        return yearsMapper.toDto(years);
    }

    /**
     *  Get all the years.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<YearsDTO> findAll() {
        log.debug("Request to get all Years");
        return yearsRepository.findAll().stream()
            .map(yearsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one years by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public YearsDTO findOne(Long id) {
        log.debug("Request to get Years : {}", id);
        Years years = yearsRepository.findOne(id);
        return yearsMapper.toDto(years);
    }

    /**
     *  Delete the  years by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Years : {}", id);
        yearsRepository.delete(id);
    }
}
