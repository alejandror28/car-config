package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.Prices;
import com.cartienda.carconfig.repository.PricesRepository;
import com.cartienda.carconfig.service.dto.PricesDTO;
import com.cartienda.carconfig.service.mapper.PricesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Prices.
 */
@Service
@Transactional
public class PricesService {

    private final Logger log = LoggerFactory.getLogger(PricesService.class);

    private final PricesRepository pricesRepository;

    private final PricesMapper pricesMapper;

    public PricesService(PricesRepository pricesRepository, PricesMapper pricesMapper) {
        this.pricesRepository = pricesRepository;
        this.pricesMapper = pricesMapper;
    }

    /**
     * Save a prices.
     *
     * @param pricesDTO the entity to save
     * @return the persisted entity
     */
    public PricesDTO save(PricesDTO pricesDTO) {
        log.debug("Request to save Prices : {}", pricesDTO);
        Prices prices = pricesMapper.toEntity(pricesDTO);
        prices = pricesRepository.save(prices);
        return pricesMapper.toDto(prices);
    }

    /**
     *  Get all the prices.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PricesDTO> findAll() {
        log.debug("Request to get all Prices");
        return pricesRepository.findAll().stream()
            .map(pricesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one prices by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PricesDTO findOne(Long id) {
        log.debug("Request to get Prices : {}", id);
        Prices prices = pricesRepository.findOne(id);
        return pricesMapper.toDto(prices);
    }

    /**
     *  Delete the  prices by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Prices : {}", id);
        pricesRepository.delete(id);
    }
}
