package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.Fields;
import com.cartienda.carconfig.repository.FieldsRepository;
import com.cartienda.carconfig.service.dto.FieldsDTO;
import com.cartienda.carconfig.service.mapper.FieldsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Fields.
 */
@Service
@Transactional
public class FieldsService {

    private final Logger log = LoggerFactory.getLogger(FieldsService.class);

    private final FieldsRepository fieldsRepository;

    private final FieldsMapper fieldsMapper;

    public FieldsService(FieldsRepository fieldsRepository, FieldsMapper fieldsMapper) {
        this.fieldsRepository = fieldsRepository;
        this.fieldsMapper = fieldsMapper;
    }

    /**
     * Save a fields.
     *
     * @param fieldsDTO the entity to save
     * @return the persisted entity
     */
    public FieldsDTO save(FieldsDTO fieldsDTO) {
        log.debug("Request to save Fields : {}", fieldsDTO);
        Fields fields = fieldsMapper.toEntity(fieldsDTO);
        fields = fieldsRepository.save(fields);
        return fieldsMapper.toDto(fields);
    }

    /**
     *  Get all the fields.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<FieldsDTO> findAll() {
        log.debug("Request to get all Fields");
        return fieldsRepository.findAll().stream()
            .map(fieldsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one fields by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public FieldsDTO findOne(Long id) {
        log.debug("Request to get Fields : {}", id);
        Fields fields = fieldsRepository.findOne(id);
        return fieldsMapper.toDto(fields);
    }

    /**
     *  Delete the  fields by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Fields : {}", id);
        fieldsRepository.delete(id);
    }
}
