package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CarBrands;
import com.cartienda.carconfig.repository.CarBrandsRepository;
import com.cartienda.carconfig.service.dto.CarBrandsDTO;
import com.cartienda.carconfig.service.mapper.CarBrandsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CarBrands.
 */
@Service
@Transactional
public class CarBrandsService {

    private final Logger log = LoggerFactory.getLogger(CarBrandsService.class);

    private final CarBrandsRepository carBrandsRepository;

    private final CarBrandsMapper carBrandsMapper;

    public CarBrandsService(CarBrandsRepository carBrandsRepository, CarBrandsMapper carBrandsMapper) {
        this.carBrandsRepository = carBrandsRepository;
        this.carBrandsMapper = carBrandsMapper;
    }

    /**
     * Save a carBrands.
     *
     * @param carBrandsDTO the entity to save
     * @return the persisted entity
     */
    public CarBrandsDTO save(CarBrandsDTO carBrandsDTO) {
        log.debug("Request to save CarBrands : {}", carBrandsDTO);
        CarBrands carBrands = carBrandsMapper.toEntity(carBrandsDTO);
        carBrands = carBrandsRepository.save(carBrands);
        return carBrandsMapper.toDto(carBrands);
    }

    /**
     *  Get all the carBrands.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CarBrandsDTO> findAll() {
        log.debug("Request to get all CarBrands");
        return carBrandsRepository.findAll().stream()
            .map(carBrandsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one carBrands by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CarBrandsDTO findOne(Long id) {
        log.debug("Request to get CarBrands : {}", id);
        CarBrands carBrands = carBrandsRepository.findOne(id);
        return carBrandsMapper.toDto(carBrands);
    }

    /**
     *  Delete the  carBrands by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarBrands : {}", id);
        carBrandsRepository.delete(id);
    }
}
