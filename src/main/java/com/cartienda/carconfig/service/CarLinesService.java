package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CarLines;
import com.cartienda.carconfig.repository.CarLinesRepository;
import com.cartienda.carconfig.service.dto.CarLinesDTO;
import com.cartienda.carconfig.service.mapper.CarLinesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CarLines.
 */
@Service
@Transactional
public class CarLinesService {

    private final Logger log = LoggerFactory.getLogger(CarLinesService.class);

    private final CarLinesRepository carLinesRepository;

    private final CarLinesMapper carLinesMapper;

    public CarLinesService(CarLinesRepository carLinesRepository, CarLinesMapper carLinesMapper) {
        this.carLinesRepository = carLinesRepository;
        this.carLinesMapper = carLinesMapper;
    }

    /**
     * Save a carLines.
     *
     * @param carLinesDTO the entity to save
     * @return the persisted entity
     */
    public CarLinesDTO save(CarLinesDTO carLinesDTO) {
        log.debug("Request to save CarLines : {}", carLinesDTO);
        CarLines carLines = carLinesMapper.toEntity(carLinesDTO);
        carLines = carLinesRepository.save(carLines);
        return carLinesMapper.toDto(carLines);
    }

    /**
     *  Get all the carLines.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CarLinesDTO> findAll() {
        log.debug("Request to get all CarLines");
        return carLinesRepository.findAll().stream()
            .map(carLinesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one carLines by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CarLinesDTO findOne(Long id) {
        log.debug("Request to get CarLines : {}", id);
        CarLines carLines = carLinesRepository.findOne(id);
        return carLinesMapper.toDto(carLines);
    }

    /**
     *  Delete the  carLines by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarLines : {}", id);
        carLinesRepository.delete(id);
    }
}
