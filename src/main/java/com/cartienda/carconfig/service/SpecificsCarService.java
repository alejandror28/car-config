package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.SpecificsCar;
import com.cartienda.carconfig.repository.SpecificsCarRepository;
import com.cartienda.carconfig.service.dto.SpecificsCarDTO;
import com.cartienda.carconfig.service.mapper.SpecificsCarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SpecificsCar.
 */
@Service
@Transactional
public class SpecificsCarService {

    private final Logger log = LoggerFactory.getLogger(SpecificsCarService.class);

    private final SpecificsCarRepository specificsCarRepository;

    private final SpecificsCarMapper specificsCarMapper;

    public SpecificsCarService(SpecificsCarRepository specificsCarRepository, SpecificsCarMapper specificsCarMapper) {
        this.specificsCarRepository = specificsCarRepository;
        this.specificsCarMapper = specificsCarMapper;
    }

    /**
     * Save a specificsCar.
     *
     * @param specificsCarDTO the entity to save
     * @return the persisted entity
     */
    public SpecificsCarDTO save(SpecificsCarDTO specificsCarDTO) {
        log.debug("Request to save SpecificsCar : {}", specificsCarDTO);
        SpecificsCar specificsCar = specificsCarMapper.toEntity(specificsCarDTO);
        specificsCar = specificsCarRepository.save(specificsCar);
        return specificsCarMapper.toDto(specificsCar);
    }

    /**
     *  Get all the specificsCars.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SpecificsCarDTO> findAll() {
        log.debug("Request to get all SpecificsCars");
        return specificsCarRepository.findAll().stream()
            .map(specificsCarMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one specificsCar by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SpecificsCarDTO findOne(Long id) {
        log.debug("Request to get SpecificsCar : {}", id);
        SpecificsCar specificsCar = specificsCarRepository.findOne(id);
        return specificsCarMapper.toDto(specificsCar);
    }

    /**
     *  Delete the  specificsCar by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SpecificsCar : {}", id);
        specificsCarRepository.delete(id);
    }
}
