package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CarbrandsCarTypes;
import com.cartienda.carconfig.repository.CarbrandsCarTypesRepository;
import com.cartienda.carconfig.service.dto.CarbrandsCarTypesDTO;
import com.cartienda.carconfig.service.mapper.CarbrandsCarTypesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CarbrandsCarTypes.
 */
@Service
@Transactional
public class CarbrandsCarTypesService {

    private final Logger log = LoggerFactory.getLogger(CarbrandsCarTypesService.class);

    private final CarbrandsCarTypesRepository carbrandsCarTypesRepository;

    private final CarbrandsCarTypesMapper carbrandsCarTypesMapper;

    public CarbrandsCarTypesService(CarbrandsCarTypesRepository carbrandsCarTypesRepository, CarbrandsCarTypesMapper carbrandsCarTypesMapper) {
        this.carbrandsCarTypesRepository = carbrandsCarTypesRepository;
        this.carbrandsCarTypesMapper = carbrandsCarTypesMapper;
    }

    /**
     * Save a carbrandsCarTypes.
     *
     * @param carbrandsCarTypesDTO the entity to save
     * @return the persisted entity
     */
    public CarbrandsCarTypesDTO save(CarbrandsCarTypesDTO carbrandsCarTypesDTO) {
        log.debug("Request to save CarbrandsCarTypes : {}", carbrandsCarTypesDTO);
        CarbrandsCarTypes carbrandsCarTypes = carbrandsCarTypesMapper.toEntity(carbrandsCarTypesDTO);
        carbrandsCarTypes = carbrandsCarTypesRepository.save(carbrandsCarTypes);
        return carbrandsCarTypesMapper.toDto(carbrandsCarTypes);
    }

    /**
     *  Get all the carbrandsCarTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CarbrandsCarTypesDTO> findAll() {
        log.debug("Request to get all CarbrandsCarTypes");
        return carbrandsCarTypesRepository.findAll().stream()
            .map(carbrandsCarTypesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one carbrandsCarTypes by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CarbrandsCarTypesDTO findOne(Long id) {
        log.debug("Request to get CarbrandsCarTypes : {}", id);
        CarbrandsCarTypes carbrandsCarTypes = carbrandsCarTypesRepository.findOne(id);
        return carbrandsCarTypesMapper.toDto(carbrandsCarTypes);
    }

    /**
     *  Delete the  carbrandsCarTypes by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarbrandsCarTypes : {}", id);
        carbrandsCarTypesRepository.delete(id);
    }
}
