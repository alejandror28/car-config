package com.cartienda.carconfig.service;

import com.cartienda.carconfig.domain.CarTypes;
import com.cartienda.carconfig.repository.CarTypesRepository;
import com.cartienda.carconfig.service.dto.CarTypesDTO;
import com.cartienda.carconfig.service.mapper.CarTypesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CarTypes.
 */
@Service
@Transactional
public class CarTypesService {

    private final Logger log = LoggerFactory.getLogger(CarTypesService.class);

    private final CarTypesRepository carTypesRepository;

    private final CarTypesMapper carTypesMapper;

    public CarTypesService(CarTypesRepository carTypesRepository, CarTypesMapper carTypesMapper) {
        this.carTypesRepository = carTypesRepository;
        this.carTypesMapper = carTypesMapper;
    }

    /**
     * Save a carTypes.
     *
     * @param carTypesDTO the entity to save
     * @return the persisted entity
     */
    public CarTypesDTO save(CarTypesDTO carTypesDTO) {
        log.debug("Request to save CarTypes : {}", carTypesDTO);
        CarTypes carTypes = carTypesMapper.toEntity(carTypesDTO);
        carTypes = carTypesRepository.save(carTypes);
        return carTypesMapper.toDto(carTypes);
    }

    /**
     *  Get all the carTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CarTypesDTO> findAll() {
        log.debug("Request to get all CarTypes");
        return carTypesRepository.findAll().stream()
            .map(carTypesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one carTypes by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CarTypesDTO findOne(Long id) {
        log.debug("Request to get CarTypes : {}", id);
        CarTypes carTypes = carTypesRepository.findOne(id);
        return carTypesMapper.toDto(carTypes);
    }

    /**
     *  Delete the  carTypes by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarTypes : {}", id);
        carTypesRepository.delete(id);
    }
}
