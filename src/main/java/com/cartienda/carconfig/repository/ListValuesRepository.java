package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.ListValues;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ListValues entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListValuesRepository extends JpaRepository<ListValues,Long> {
    
}
