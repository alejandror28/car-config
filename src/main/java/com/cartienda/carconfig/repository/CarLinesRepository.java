package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CarLines;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CarLines entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarLinesRepository extends JpaRepository<CarLines,Long> {
    
}
