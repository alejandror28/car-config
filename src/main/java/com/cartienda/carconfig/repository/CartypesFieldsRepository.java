package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CartypesFields;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CartypesFields entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartypesFieldsRepository extends JpaRepository<CartypesFields,Long> {
    
}
