package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CarTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CarTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarTypesRepository extends JpaRepository<CarTypes,Long> {
    
}
