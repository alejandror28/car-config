package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.SpecificsCar;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SpecificsCar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecificsCarRepository extends JpaRepository<SpecificsCar,Long> {
    
}
