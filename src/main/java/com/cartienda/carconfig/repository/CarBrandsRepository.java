package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CarBrands;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CarBrands entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarBrandsRepository extends JpaRepository<CarBrands,Long> {
    
}
