package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.Prices;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Prices entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PricesRepository extends JpaRepository<Prices,Long> {
    
}
