package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CarVersions;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CarVersions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarVersionsRepository extends JpaRepository<CarVersions,Long> {
    
}
