package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.CarbrandsCarTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CarbrandsCarTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarbrandsCarTypesRepository extends JpaRepository<CarbrandsCarTypes,Long> {
    
}
