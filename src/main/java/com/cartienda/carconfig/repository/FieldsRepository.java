package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.Fields;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Fields entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FieldsRepository extends JpaRepository<Fields,Long> {
    
}
