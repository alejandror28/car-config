package com.cartienda.carconfig.repository;

import com.cartienda.carconfig.domain.Years;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Years entity.
 */
@SuppressWarnings("unused")
@Repository
public interface YearsRepository extends JpaRepository<Years,Long> {
    
}
