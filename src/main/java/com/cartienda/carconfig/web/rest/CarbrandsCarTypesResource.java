package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CarbrandsCarTypesService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CarbrandsCarTypesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CarbrandsCarTypes.
 */
@RestController
@RequestMapping("/api")
public class CarbrandsCarTypesResource {

    private final Logger log = LoggerFactory.getLogger(CarbrandsCarTypesResource.class);

    private static final String ENTITY_NAME = "carbrandsCarTypes";

    private final CarbrandsCarTypesService carbrandsCarTypesService;

    public CarbrandsCarTypesResource(CarbrandsCarTypesService carbrandsCarTypesService) {
        this.carbrandsCarTypesService = carbrandsCarTypesService;
    }

    /**
     * POST  /carbrands-car-types : Create a new carbrandsCarTypes.
     *
     * @param carbrandsCarTypesDTO the carbrandsCarTypesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carbrandsCarTypesDTO, or with status 400 (Bad Request) if the carbrandsCarTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/carbrands-car-types")
    @Timed
    public ResponseEntity<CarbrandsCarTypesDTO> createCarbrandsCarTypes(@RequestBody CarbrandsCarTypesDTO carbrandsCarTypesDTO) throws URISyntaxException {
        log.debug("REST request to save CarbrandsCarTypes : {}", carbrandsCarTypesDTO);
        if (carbrandsCarTypesDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new carbrandsCarTypes cannot already have an ID")).body(null);
        }
        CarbrandsCarTypesDTO result = carbrandsCarTypesService.save(carbrandsCarTypesDTO);
        return ResponseEntity.created(new URI("/api/carbrands-car-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /carbrands-car-types : Updates an existing carbrandsCarTypes.
     *
     * @param carbrandsCarTypesDTO the carbrandsCarTypesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carbrandsCarTypesDTO,
     * or with status 400 (Bad Request) if the carbrandsCarTypesDTO is not valid,
     * or with status 500 (Internal Server Error) if the carbrandsCarTypesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/carbrands-car-types")
    @Timed
    public ResponseEntity<CarbrandsCarTypesDTO> updateCarbrandsCarTypes(@RequestBody CarbrandsCarTypesDTO carbrandsCarTypesDTO) throws URISyntaxException {
        log.debug("REST request to update CarbrandsCarTypes : {}", carbrandsCarTypesDTO);
        if (carbrandsCarTypesDTO.getId() == null) {
            return createCarbrandsCarTypes(carbrandsCarTypesDTO);
        }
        CarbrandsCarTypesDTO result = carbrandsCarTypesService.save(carbrandsCarTypesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, carbrandsCarTypesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /carbrands-car-types : get all the carbrandsCarTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of carbrandsCarTypes in body
     */
    @GetMapping("/carbrands-car-types")
    @Timed
    public List<CarbrandsCarTypesDTO> getAllCarbrandsCarTypes() {
        log.debug("REST request to get all CarbrandsCarTypes");
        return carbrandsCarTypesService.findAll();
    }

    /**
     * GET  /carbrands-car-types/:id : get the "id" carbrandsCarTypes.
     *
     * @param id the id of the carbrandsCarTypesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carbrandsCarTypesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/carbrands-car-types/{id}")
    @Timed
    public ResponseEntity<CarbrandsCarTypesDTO> getCarbrandsCarTypes(@PathVariable Long id) {
        log.debug("REST request to get CarbrandsCarTypes : {}", id);
        CarbrandsCarTypesDTO carbrandsCarTypesDTO = carbrandsCarTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(carbrandsCarTypesDTO));
    }

    /**
     * DELETE  /carbrands-car-types/:id : delete the "id" carbrandsCarTypes.
     *
     * @param id the id of the carbrandsCarTypesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/carbrands-car-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarbrandsCarTypes(@PathVariable Long id) {
        log.debug("REST request to delete CarbrandsCarTypes : {}", id);
        carbrandsCarTypesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
