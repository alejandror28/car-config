package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.PricesService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.PricesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Prices.
 */
@RestController
@RequestMapping("/api")
public class PricesResource {

    private final Logger log = LoggerFactory.getLogger(PricesResource.class);

    private static final String ENTITY_NAME = "prices";

    private final PricesService pricesService;

    public PricesResource(PricesService pricesService) {
        this.pricesService = pricesService;
    }

    /**
     * POST  /prices : Create a new prices.
     *
     * @param pricesDTO the pricesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pricesDTO, or with status 400 (Bad Request) if the prices has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prices")
    @Timed
    public ResponseEntity<PricesDTO> createPrices(@Valid @RequestBody PricesDTO pricesDTO) throws URISyntaxException {
        log.debug("REST request to save Prices : {}", pricesDTO);
        if (pricesDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prices cannot already have an ID")).body(null);
        }
        PricesDTO result = pricesService.save(pricesDTO);
        return ResponseEntity.created(new URI("/api/prices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /prices : Updates an existing prices.
     *
     * @param pricesDTO the pricesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pricesDTO,
     * or with status 400 (Bad Request) if the pricesDTO is not valid,
     * or with status 500 (Internal Server Error) if the pricesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prices")
    @Timed
    public ResponseEntity<PricesDTO> updatePrices(@Valid @RequestBody PricesDTO pricesDTO) throws URISyntaxException {
        log.debug("REST request to update Prices : {}", pricesDTO);
        if (pricesDTO.getId() == null) {
            return createPrices(pricesDTO);
        }
        PricesDTO result = pricesService.save(pricesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pricesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /prices : get all the prices.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of prices in body
     */
    @GetMapping("/prices")
    @Timed
    public List<PricesDTO> getAllPrices() {
        log.debug("REST request to get all Prices");
        return pricesService.findAll();
    }

    /**
     * GET  /prices/:id : get the "id" prices.
     *
     * @param id the id of the pricesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pricesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prices/{id}")
    @Timed
    public ResponseEntity<PricesDTO> getPrices(@PathVariable Long id) {
        log.debug("REST request to get Prices : {}", id);
        PricesDTO pricesDTO = pricesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pricesDTO));
    }

    /**
     * DELETE  /prices/:id : delete the "id" prices.
     *
     * @param id the id of the pricesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prices/{id}")
    @Timed
    public ResponseEntity<Void> deletePrices(@PathVariable Long id) {
        log.debug("REST request to delete Prices : {}", id);
        pricesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
