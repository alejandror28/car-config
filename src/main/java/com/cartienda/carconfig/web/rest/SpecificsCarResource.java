package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.SpecificsCarService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.SpecificsCarDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SpecificsCar.
 */
@RestController
@RequestMapping("/api")
public class SpecificsCarResource {

    private final Logger log = LoggerFactory.getLogger(SpecificsCarResource.class);

    private static final String ENTITY_NAME = "specificsCar";

    private final SpecificsCarService specificsCarService;

    public SpecificsCarResource(SpecificsCarService specificsCarService) {
        this.specificsCarService = specificsCarService;
    }

    /**
     * POST  /specifics-cars : Create a new specificsCar.
     *
     * @param specificsCarDTO the specificsCarDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new specificsCarDTO, or with status 400 (Bad Request) if the specificsCar has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/specifics-cars")
    @Timed
    public ResponseEntity<SpecificsCarDTO> createSpecificsCar(@RequestBody SpecificsCarDTO specificsCarDTO) throws URISyntaxException {
        log.debug("REST request to save SpecificsCar : {}", specificsCarDTO);
        if (specificsCarDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new specificsCar cannot already have an ID")).body(null);
        }
        SpecificsCarDTO result = specificsCarService.save(specificsCarDTO);
        return ResponseEntity.created(new URI("/api/specifics-cars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specifics-cars : Updates an existing specificsCar.
     *
     * @param specificsCarDTO the specificsCarDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated specificsCarDTO,
     * or with status 400 (Bad Request) if the specificsCarDTO is not valid,
     * or with status 500 (Internal Server Error) if the specificsCarDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/specifics-cars")
    @Timed
    public ResponseEntity<SpecificsCarDTO> updateSpecificsCar(@RequestBody SpecificsCarDTO specificsCarDTO) throws URISyntaxException {
        log.debug("REST request to update SpecificsCar : {}", specificsCarDTO);
        if (specificsCarDTO.getId() == null) {
            return createSpecificsCar(specificsCarDTO);
        }
        SpecificsCarDTO result = specificsCarService.save(specificsCarDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, specificsCarDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specifics-cars : get all the specificsCars.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of specificsCars in body
     */
    @GetMapping("/specifics-cars")
    @Timed
    public List<SpecificsCarDTO> getAllSpecificsCars() {
        log.debug("REST request to get all SpecificsCars");
        return specificsCarService.findAll();
    }

    /**
     * GET  /specifics-cars/:id : get the "id" specificsCar.
     *
     * @param id the id of the specificsCarDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the specificsCarDTO, or with status 404 (Not Found)
     */
    @GetMapping("/specifics-cars/{id}")
    @Timed
    public ResponseEntity<SpecificsCarDTO> getSpecificsCar(@PathVariable Long id) {
        log.debug("REST request to get SpecificsCar : {}", id);
        SpecificsCarDTO specificsCarDTO = specificsCarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(specificsCarDTO));
    }

    /**
     * DELETE  /specifics-cars/:id : delete the "id" specificsCar.
     *
     * @param id the id of the specificsCarDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/specifics-cars/{id}")
    @Timed
    public ResponseEntity<Void> deleteSpecificsCar(@PathVariable Long id) {
        log.debug("REST request to delete SpecificsCar : {}", id);
        specificsCarService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
