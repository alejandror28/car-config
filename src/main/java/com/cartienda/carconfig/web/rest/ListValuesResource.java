package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.ListValuesService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.ListValuesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ListValues.
 */
@RestController
@RequestMapping("/api")
public class ListValuesResource {

    private final Logger log = LoggerFactory.getLogger(ListValuesResource.class);

    private static final String ENTITY_NAME = "listValues";

    private final ListValuesService listValuesService;

    public ListValuesResource(ListValuesService listValuesService) {
        this.listValuesService = listValuesService;
    }

    /**
     * POST  /list-values : Create a new listValues.
     *
     * @param listValuesDTO the listValuesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new listValuesDTO, or with status 400 (Bad Request) if the listValues has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/list-values")
    @Timed
    public ResponseEntity<ListValuesDTO> createListValues(@Valid @RequestBody ListValuesDTO listValuesDTO) throws URISyntaxException {
        log.debug("REST request to save ListValues : {}", listValuesDTO);
        if (listValuesDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new listValues cannot already have an ID")).body(null);
        }
        ListValuesDTO result = listValuesService.save(listValuesDTO);
        return ResponseEntity.created(new URI("/api/list-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /list-values : Updates an existing listValues.
     *
     * @param listValuesDTO the listValuesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated listValuesDTO,
     * or with status 400 (Bad Request) if the listValuesDTO is not valid,
     * or with status 500 (Internal Server Error) if the listValuesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/list-values")
    @Timed
    public ResponseEntity<ListValuesDTO> updateListValues(@Valid @RequestBody ListValuesDTO listValuesDTO) throws URISyntaxException {
        log.debug("REST request to update ListValues : {}", listValuesDTO);
        if (listValuesDTO.getId() == null) {
            return createListValues(listValuesDTO);
        }
        ListValuesDTO result = listValuesService.save(listValuesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, listValuesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /list-values : get all the listValues.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of listValues in body
     */
    @GetMapping("/list-values")
    @Timed
    public List<ListValuesDTO> getAllListValues() {
        log.debug("REST request to get all ListValues");
        return listValuesService.findAll();
    }

    /**
     * GET  /list-values/:id : get the "id" listValues.
     *
     * @param id the id of the listValuesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the listValuesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/list-values/{id}")
    @Timed
    public ResponseEntity<ListValuesDTO> getListValues(@PathVariable Long id) {
        log.debug("REST request to get ListValues : {}", id);
        ListValuesDTO listValuesDTO = listValuesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(listValuesDTO));
    }

    /**
     * DELETE  /list-values/:id : delete the "id" listValues.
     *
     * @param id the id of the listValuesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/list-values/{id}")
    @Timed
    public ResponseEntity<Void> deleteListValues(@PathVariable Long id) {
        log.debug("REST request to delete ListValues : {}", id);
        listValuesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
