package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CarBrandsService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CarBrandsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CarBrands.
 */
@RestController
@RequestMapping("/api")
public class CarBrandsResource {

    private final Logger log = LoggerFactory.getLogger(CarBrandsResource.class);

    private static final String ENTITY_NAME = "carBrands";

    private final CarBrandsService carBrandsService;

    public CarBrandsResource(CarBrandsService carBrandsService) {
        this.carBrandsService = carBrandsService;
    }

    /**
     * POST  /car-brands : Create a new carBrands.
     *
     * @param carBrandsDTO the carBrandsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carBrandsDTO, or with status 400 (Bad Request) if the carBrands has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-brands")
    @Timed
    public ResponseEntity<CarBrandsDTO> createCarBrands(@Valid @RequestBody CarBrandsDTO carBrandsDTO) throws URISyntaxException {
        log.debug("REST request to save CarBrands : {}", carBrandsDTO);
        if (carBrandsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new carBrands cannot already have an ID")).body(null);
        }
        CarBrandsDTO result = carBrandsService.save(carBrandsDTO);
        return ResponseEntity.created(new URI("/api/car-brands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-brands : Updates an existing carBrands.
     *
     * @param carBrandsDTO the carBrandsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carBrandsDTO,
     * or with status 400 (Bad Request) if the carBrandsDTO is not valid,
     * or with status 500 (Internal Server Error) if the carBrandsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-brands")
    @Timed
    public ResponseEntity<CarBrandsDTO> updateCarBrands(@Valid @RequestBody CarBrandsDTO carBrandsDTO) throws URISyntaxException {
        log.debug("REST request to update CarBrands : {}", carBrandsDTO);
        if (carBrandsDTO.getId() == null) {
            return createCarBrands(carBrandsDTO);
        }
        CarBrandsDTO result = carBrandsService.save(carBrandsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, carBrandsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-brands : get all the carBrands.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of carBrands in body
     */
    @GetMapping("/car-brands")
    @Timed
    public List<CarBrandsDTO> getAllCarBrands() {
        log.debug("REST request to get all CarBrands");
        return carBrandsService.findAll();
    }

    /**
     * GET  /car-brands/:id : get the "id" carBrands.
     *
     * @param id the id of the carBrandsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carBrandsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/car-brands/{id}")
    @Timed
    public ResponseEntity<CarBrandsDTO> getCarBrands(@PathVariable Long id) {
        log.debug("REST request to get CarBrands : {}", id);
        CarBrandsDTO carBrandsDTO = carBrandsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(carBrandsDTO));
    }

    /**
     * DELETE  /car-brands/:id : delete the "id" carBrands.
     *
     * @param id the id of the carBrandsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-brands/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarBrands(@PathVariable Long id) {
        log.debug("REST request to delete CarBrands : {}", id);
        carBrandsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
