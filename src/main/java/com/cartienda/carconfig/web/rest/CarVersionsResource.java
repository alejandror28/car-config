package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CarVersionsService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CarVersionsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CarVersions.
 */
@RestController
@RequestMapping("/api")
public class CarVersionsResource {

    private final Logger log = LoggerFactory.getLogger(CarVersionsResource.class);

    private static final String ENTITY_NAME = "carVersions";

    private final CarVersionsService carVersionsService;

    public CarVersionsResource(CarVersionsService carVersionsService) {
        this.carVersionsService = carVersionsService;
    }

    /**
     * POST  /car-versions : Create a new carVersions.
     *
     * @param carVersionsDTO the carVersionsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carVersionsDTO, or with status 400 (Bad Request) if the carVersions has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-versions")
    @Timed
    public ResponseEntity<CarVersionsDTO> createCarVersions(@Valid @RequestBody CarVersionsDTO carVersionsDTO) throws URISyntaxException {
        log.debug("REST request to save CarVersions : {}", carVersionsDTO);
        if (carVersionsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new carVersions cannot already have an ID")).body(null);
        }
        CarVersionsDTO result = carVersionsService.save(carVersionsDTO);
        return ResponseEntity.created(new URI("/api/car-versions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-versions : Updates an existing carVersions.
     *
     * @param carVersionsDTO the carVersionsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carVersionsDTO,
     * or with status 400 (Bad Request) if the carVersionsDTO is not valid,
     * or with status 500 (Internal Server Error) if the carVersionsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-versions")
    @Timed
    public ResponseEntity<CarVersionsDTO> updateCarVersions(@Valid @RequestBody CarVersionsDTO carVersionsDTO) throws URISyntaxException {
        log.debug("REST request to update CarVersions : {}", carVersionsDTO);
        if (carVersionsDTO.getId() == null) {
            return createCarVersions(carVersionsDTO);
        }
        CarVersionsDTO result = carVersionsService.save(carVersionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, carVersionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-versions : get all the carVersions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of carVersions in body
     */
    @GetMapping("/car-versions")
    @Timed
    public List<CarVersionsDTO> getAllCarVersions() {
        log.debug("REST request to get all CarVersions");
        return carVersionsService.findAll();
    }

    /**
     * GET  /car-versions/:id : get the "id" carVersions.
     *
     * @param id the id of the carVersionsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carVersionsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/car-versions/{id}")
    @Timed
    public ResponseEntity<CarVersionsDTO> getCarVersions(@PathVariable Long id) {
        log.debug("REST request to get CarVersions : {}", id);
        CarVersionsDTO carVersionsDTO = carVersionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(carVersionsDTO));
    }

    /**
     * DELETE  /car-versions/:id : delete the "id" carVersions.
     *
     * @param id the id of the carVersionsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-versions/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarVersions(@PathVariable Long id) {
        log.debug("REST request to delete CarVersions : {}", id);
        carVersionsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
