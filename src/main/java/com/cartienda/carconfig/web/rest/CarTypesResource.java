package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CarTypesService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CarTypesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CarTypes.
 */
@RestController
@RequestMapping("/api")
public class CarTypesResource {

    private final Logger log = LoggerFactory.getLogger(CarTypesResource.class);

    private static final String ENTITY_NAME = "carTypes";

    private final CarTypesService carTypesService;

    public CarTypesResource(CarTypesService carTypesService) {
        this.carTypesService = carTypesService;
    }

    /**
     * POST  /car-types : Create a new carTypes.
     *
     * @param carTypesDTO the carTypesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carTypesDTO, or with status 400 (Bad Request) if the carTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-types")
    @Timed
    public ResponseEntity<CarTypesDTO> createCarTypes(@Valid @RequestBody CarTypesDTO carTypesDTO) throws URISyntaxException {
        log.debug("REST request to save CarTypes : {}", carTypesDTO);
        if (carTypesDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new carTypes cannot already have an ID")).body(null);
        }
        CarTypesDTO result = carTypesService.save(carTypesDTO);
        return ResponseEntity.created(new URI("/api/car-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-types : Updates an existing carTypes.
     *
     * @param carTypesDTO the carTypesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carTypesDTO,
     * or with status 400 (Bad Request) if the carTypesDTO is not valid,
     * or with status 500 (Internal Server Error) if the carTypesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-types")
    @Timed
    public ResponseEntity<CarTypesDTO> updateCarTypes(@Valid @RequestBody CarTypesDTO carTypesDTO) throws URISyntaxException {
        log.debug("REST request to update CarTypes : {}", carTypesDTO);
        if (carTypesDTO.getId() == null) {
            return createCarTypes(carTypesDTO);
        }
        CarTypesDTO result = carTypesService.save(carTypesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, carTypesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-types : get all the carTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of carTypes in body
     */
    @GetMapping("/car-types")
    @Timed
    public List<CarTypesDTO> getAllCarTypes() {
        log.debug("REST request to get all CarTypes");
        return carTypesService.findAll();
    }

    /**
     * GET  /car-types/:id : get the "id" carTypes.
     *
     * @param id the id of the carTypesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carTypesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/car-types/{id}")
    @Timed
    public ResponseEntity<CarTypesDTO> getCarTypes(@PathVariable Long id) {
        log.debug("REST request to get CarTypes : {}", id);
        CarTypesDTO carTypesDTO = carTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(carTypesDTO));
    }

    /**
     * DELETE  /car-types/:id : delete the "id" carTypes.
     *
     * @param id the id of the carTypesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarTypes(@PathVariable Long id) {
        log.debug("REST request to delete CarTypes : {}", id);
        carTypesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
