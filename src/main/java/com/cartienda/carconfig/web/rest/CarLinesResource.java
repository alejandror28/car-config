package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CarLinesService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CarLinesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CarLines.
 */
@RestController
@RequestMapping("/api")
public class CarLinesResource {

    private final Logger log = LoggerFactory.getLogger(CarLinesResource.class);

    private static final String ENTITY_NAME = "carLines";

    private final CarLinesService carLinesService;

    public CarLinesResource(CarLinesService carLinesService) {
        this.carLinesService = carLinesService;
    }

    /**
     * POST  /car-lines : Create a new carLines.
     *
     * @param carLinesDTO the carLinesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carLinesDTO, or with status 400 (Bad Request) if the carLines has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-lines")
    @Timed
    public ResponseEntity<CarLinesDTO> createCarLines(@Valid @RequestBody CarLinesDTO carLinesDTO) throws URISyntaxException {
        log.debug("REST request to save CarLines : {}", carLinesDTO);
        if (carLinesDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new carLines cannot already have an ID")).body(null);
        }
        CarLinesDTO result = carLinesService.save(carLinesDTO);
        return ResponseEntity.created(new URI("/api/car-lines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-lines : Updates an existing carLines.
     *
     * @param carLinesDTO the carLinesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carLinesDTO,
     * or with status 400 (Bad Request) if the carLinesDTO is not valid,
     * or with status 500 (Internal Server Error) if the carLinesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-lines")
    @Timed
    public ResponseEntity<CarLinesDTO> updateCarLines(@Valid @RequestBody CarLinesDTO carLinesDTO) throws URISyntaxException {
        log.debug("REST request to update CarLines : {}", carLinesDTO);
        if (carLinesDTO.getId() == null) {
            return createCarLines(carLinesDTO);
        }
        CarLinesDTO result = carLinesService.save(carLinesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, carLinesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-lines : get all the carLines.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of carLines in body
     */
    @GetMapping("/car-lines")
    @Timed
    public List<CarLinesDTO> getAllCarLines() {
        log.debug("REST request to get all CarLines");
        return carLinesService.findAll();
    }

    /**
     * GET  /car-lines/:id : get the "id" carLines.
     *
     * @param id the id of the carLinesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carLinesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/car-lines/{id}")
    @Timed
    public ResponseEntity<CarLinesDTO> getCarLines(@PathVariable Long id) {
        log.debug("REST request to get CarLines : {}", id);
        CarLinesDTO carLinesDTO = carLinesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(carLinesDTO));
    }

    /**
     * DELETE  /car-lines/:id : delete the "id" carLines.
     *
     * @param id the id of the carLinesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-lines/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarLines(@PathVariable Long id) {
        log.debug("REST request to delete CarLines : {}", id);
        carLinesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
