package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.YearsService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.YearsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Years.
 */
@RestController
@RequestMapping("/api")
public class YearsResource {

    private final Logger log = LoggerFactory.getLogger(YearsResource.class);

    private static final String ENTITY_NAME = "years";

    private final YearsService yearsService;

    public YearsResource(YearsService yearsService) {
        this.yearsService = yearsService;
    }

    /**
     * POST  /years : Create a new years.
     *
     * @param yearsDTO the yearsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new yearsDTO, or with status 400 (Bad Request) if the years has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/years")
    @Timed
    public ResponseEntity<YearsDTO> createYears(@Valid @RequestBody YearsDTO yearsDTO) throws URISyntaxException {
        log.debug("REST request to save Years : {}", yearsDTO);
        if (yearsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new years cannot already have an ID")).body(null);
        }
        YearsDTO result = yearsService.save(yearsDTO);
        return ResponseEntity.created(new URI("/api/years/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /years : Updates an existing years.
     *
     * @param yearsDTO the yearsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated yearsDTO,
     * or with status 400 (Bad Request) if the yearsDTO is not valid,
     * or with status 500 (Internal Server Error) if the yearsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/years")
    @Timed
    public ResponseEntity<YearsDTO> updateYears(@Valid @RequestBody YearsDTO yearsDTO) throws URISyntaxException {
        log.debug("REST request to update Years : {}", yearsDTO);
        if (yearsDTO.getId() == null) {
            return createYears(yearsDTO);
        }
        YearsDTO result = yearsService.save(yearsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, yearsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /years : get all the years.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of years in body
     */
    @GetMapping("/years")
    @Timed
    public List<YearsDTO> getAllYears() {
        log.debug("REST request to get all Years");
        return yearsService.findAll();
    }

    /**
     * GET  /years/:id : get the "id" years.
     *
     * @param id the id of the yearsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the yearsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/years/{id}")
    @Timed
    public ResponseEntity<YearsDTO> getYears(@PathVariable Long id) {
        log.debug("REST request to get Years : {}", id);
        YearsDTO yearsDTO = yearsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(yearsDTO));
    }

    /**
     * DELETE  /years/:id : delete the "id" years.
     *
     * @param id the id of the yearsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/years/{id}")
    @Timed
    public ResponseEntity<Void> deleteYears(@PathVariable Long id) {
        log.debug("REST request to delete Years : {}", id);
        yearsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
