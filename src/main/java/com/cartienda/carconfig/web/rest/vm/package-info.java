/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cartienda.carconfig.web.rest.vm;
