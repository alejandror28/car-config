package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.FieldsService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.FieldsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Fields.
 */
@RestController
@RequestMapping("/api")
public class FieldsResource {

    private final Logger log = LoggerFactory.getLogger(FieldsResource.class);

    private static final String ENTITY_NAME = "fields";

    private final FieldsService fieldsService;

    public FieldsResource(FieldsService fieldsService) {
        this.fieldsService = fieldsService;
    }

    /**
     * POST  /fields : Create a new fields.
     *
     * @param fieldsDTO the fieldsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fieldsDTO, or with status 400 (Bad Request) if the fields has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/fields")
    @Timed
    public ResponseEntity<FieldsDTO> createFields(@Valid @RequestBody FieldsDTO fieldsDTO) throws URISyntaxException {
        log.debug("REST request to save Fields : {}", fieldsDTO);
        if (fieldsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fields cannot already have an ID")).body(null);
        }
        FieldsDTO result = fieldsService.save(fieldsDTO);
        return ResponseEntity.created(new URI("/api/fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fields : Updates an existing fields.
     *
     * @param fieldsDTO the fieldsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fieldsDTO,
     * or with status 400 (Bad Request) if the fieldsDTO is not valid,
     * or with status 500 (Internal Server Error) if the fieldsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/fields")
    @Timed
    public ResponseEntity<FieldsDTO> updateFields(@Valid @RequestBody FieldsDTO fieldsDTO) throws URISyntaxException {
        log.debug("REST request to update Fields : {}", fieldsDTO);
        if (fieldsDTO.getId() == null) {
            return createFields(fieldsDTO);
        }
        FieldsDTO result = fieldsService.save(fieldsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fieldsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fields : get all the fields.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fields in body
     */
    @GetMapping("/fields")
    @Timed
    public List<FieldsDTO> getAllFields() {
        log.debug("REST request to get all Fields");
        return fieldsService.findAll();
    }

    /**
     * GET  /fields/:id : get the "id" fields.
     *
     * @param id the id of the fieldsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fieldsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/fields/{id}")
    @Timed
    public ResponseEntity<FieldsDTO> getFields(@PathVariable Long id) {
        log.debug("REST request to get Fields : {}", id);
        FieldsDTO fieldsDTO = fieldsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fieldsDTO));
    }

    /**
     * DELETE  /fields/:id : delete the "id" fields.
     *
     * @param id the id of the fieldsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteFields(@PathVariable Long id) {
        log.debug("REST request to delete Fields : {}", id);
        fieldsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
