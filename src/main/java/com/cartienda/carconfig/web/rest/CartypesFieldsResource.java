package com.cartienda.carconfig.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.carconfig.service.CartypesFieldsService;
import com.cartienda.carconfig.web.rest.util.HeaderUtil;
import com.cartienda.carconfig.service.dto.CartypesFieldsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CartypesFields.
 */
@RestController
@RequestMapping("/api")
public class CartypesFieldsResource {

    private final Logger log = LoggerFactory.getLogger(CartypesFieldsResource.class);

    private static final String ENTITY_NAME = "cartypesFields";

    private final CartypesFieldsService cartypesFieldsService;

    public CartypesFieldsResource(CartypesFieldsService cartypesFieldsService) {
        this.cartypesFieldsService = cartypesFieldsService;
    }

    /**
     * POST  /cartypes-fields : Create a new cartypesFields.
     *
     * @param cartypesFieldsDTO the cartypesFieldsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cartypesFieldsDTO, or with status 400 (Bad Request) if the cartypesFields has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cartypes-fields")
    @Timed
    public ResponseEntity<CartypesFieldsDTO> createCartypesFields(@RequestBody CartypesFieldsDTO cartypesFieldsDTO) throws URISyntaxException {
        log.debug("REST request to save CartypesFields : {}", cartypesFieldsDTO);
        if (cartypesFieldsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new cartypesFields cannot already have an ID")).body(null);
        }
        CartypesFieldsDTO result = cartypesFieldsService.save(cartypesFieldsDTO);
        return ResponseEntity.created(new URI("/api/cartypes-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cartypes-fields : Updates an existing cartypesFields.
     *
     * @param cartypesFieldsDTO the cartypesFieldsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cartypesFieldsDTO,
     * or with status 400 (Bad Request) if the cartypesFieldsDTO is not valid,
     * or with status 500 (Internal Server Error) if the cartypesFieldsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cartypes-fields")
    @Timed
    public ResponseEntity<CartypesFieldsDTO> updateCartypesFields(@RequestBody CartypesFieldsDTO cartypesFieldsDTO) throws URISyntaxException {
        log.debug("REST request to update CartypesFields : {}", cartypesFieldsDTO);
        if (cartypesFieldsDTO.getId() == null) {
            return createCartypesFields(cartypesFieldsDTO);
        }
        CartypesFieldsDTO result = cartypesFieldsService.save(cartypesFieldsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cartypesFieldsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cartypes-fields : get all the cartypesFields.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cartypesFields in body
     */
    @GetMapping("/cartypes-fields")
    @Timed
    public List<CartypesFieldsDTO> getAllCartypesFields() {
        log.debug("REST request to get all CartypesFields");
        return cartypesFieldsService.findAll();
    }

    /**
     * GET  /cartypes-fields/:id : get the "id" cartypesFields.
     *
     * @param id the id of the cartypesFieldsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cartypesFieldsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/cartypes-fields/{id}")
    @Timed
    public ResponseEntity<CartypesFieldsDTO> getCartypesFields(@PathVariable Long id) {
        log.debug("REST request to get CartypesFields : {}", id);
        CartypesFieldsDTO cartypesFieldsDTO = cartypesFieldsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cartypesFieldsDTO));
    }

    /**
     * DELETE  /cartypes-fields/:id : delete the "id" cartypesFields.
     *
     * @param id the id of the cartypesFieldsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cartypes-fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteCartypesFields(@PathVariable Long id) {
        log.debug("REST request to delete CartypesFields : {}", id);
        cartypesFieldsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
