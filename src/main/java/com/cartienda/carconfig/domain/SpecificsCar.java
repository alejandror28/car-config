package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A SpecificsCar.
 */
@Entity
@Table(name = "specifics_car")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpecificsCar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "specificsCar")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Prices> prices = new HashSet<>();

    @ManyToOne
    private CarTypes carTypes;

    @ManyToOne
    private CarBrands carBrands;

    @ManyToOne
    private CarLines carLines;

    @ManyToOne
    private CarVersions carVersions;

    @ManyToOne
    private Years years;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Prices> getPrices() {
        return prices;
    }

    public SpecificsCar prices(Set<Prices> prices) {
        this.prices = prices;
        return this;
    }

    public SpecificsCar addPrice(Prices prices) {
        this.prices.add(prices);
        prices.setSpecificsCar(this);
        return this;
    }

    public SpecificsCar removePrice(Prices prices) {
        this.prices.remove(prices);
        prices.setSpecificsCar(null);
        return this;
    }

    public void setPrices(Set<Prices> prices) {
        this.prices = prices;
    }

    public CarTypes getCarTypes() {
        return carTypes;
    }

    public SpecificsCar carTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
        return this;
    }

    public void setCarTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
    }

    public CarBrands getCarBrands() {
        return carBrands;
    }

    public SpecificsCar carBrands(CarBrands carBrands) {
        this.carBrands = carBrands;
        return this;
    }

    public void setCarBrands(CarBrands carBrands) {
        this.carBrands = carBrands;
    }

    public CarLines getCarLines() {
        return carLines;
    }

    public SpecificsCar carLines(CarLines carLines) {
        this.carLines = carLines;
        return this;
    }

    public void setCarLines(CarLines carLines) {
        this.carLines = carLines;
    }

    public CarVersions getCarVersions() {
        return carVersions;
    }

    public SpecificsCar carVersions(CarVersions carVersions) {
        this.carVersions = carVersions;
        return this;
    }

    public void setCarVersions(CarVersions carVersions) {
        this.carVersions = carVersions;
    }

    public Years getYears() {
        return years;
    }

    public SpecificsCar years(Years years) {
        this.years = years;
        return this;
    }

    public void setYears(Years years) {
        this.years = years;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpecificsCar specificsCar = (SpecificsCar) o;
        if (specificsCar.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), specificsCar.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpecificsCar{" +
            "id=" + getId() +
            "}";
    }
}
