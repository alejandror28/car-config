package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Years.
 */
@Entity
@Table(name = "years")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Years implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_year", nullable = false)
    private String year;

    @OneToMany(mappedBy = "years")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpecificsCar> specificsCars = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public Years year(String year) {
        this.year = year;
        return this;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Set<SpecificsCar> getSpecificsCars() {
        return specificsCars;
    }

    public Years specificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
        return this;
    }

    public Years addSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.add(specificsCar);
        specificsCar.setYears(this);
        return this;
    }

    public Years removeSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.remove(specificsCar);
        specificsCar.setYears(null);
        return this;
    }

    public void setSpecificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Years years = (Years) o;
        if (years.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), years.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Years{" +
            "id=" + getId() +
            ", year='" + getYear() + "'" +
            "}";
    }
}
