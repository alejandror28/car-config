package com.cartienda.carconfig.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ListValues.
 */
@Entity
@Table(name = "list_values")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ListValues implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 60)
    @Column(name = "list_type", length = 60, nullable = false)
    private String listType;

    @NotNull
    @Size(max = 60)
    @Column(name = "jhi_value", length = 60, nullable = false)
    private String value;

    @ManyToOne
    private CartypesFields carTypesFields;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getListType() {
        return listType;
    }

    public ListValues listType(String listType) {
        this.listType = listType;
        return this;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public String getValue() {
        return value;
    }

    public ListValues value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CartypesFields getCarTypesFields() {
        return carTypesFields;
    }

    public ListValues carTypesFields(CartypesFields cartypesFields) {
        this.carTypesFields = cartypesFields;
        return this;
    }

    public void setCarTypesFields(CartypesFields cartypesFields) {
        this.carTypesFields = cartypesFields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ListValues listValues = (ListValues) o;
        if (listValues.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), listValues.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ListValues{" +
            "id=" + getId() +
            ", listType='" + getListType() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
