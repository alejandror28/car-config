package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CarbrandsCarTypes.
 */
@Entity
@Table(name = "carbrands_car_types")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CarbrandsCarTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "carbrandsCarTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CarLines> carLines = new HashSet<>();

    @ManyToOne
    private CarBrands carBrands;

    @ManyToOne
    private CarTypes carTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<CarLines> getCarLines() {
        return carLines;
    }

    public CarbrandsCarTypes carLines(Set<CarLines> carLines) {
        this.carLines = carLines;
        return this;
    }

    public CarbrandsCarTypes addCarLines(CarLines carLines) {
        this.carLines.add(carLines);
        carLines.setCarbrandsCarTypes(this);
        return this;
    }

    public CarbrandsCarTypes removeCarLines(CarLines carLines) {
        this.carLines.remove(carLines);
        carLines.setCarbrandsCarTypes(null);
        return this;
    }

    public void setCarLines(Set<CarLines> carLines) {
        this.carLines = carLines;
    }

    public CarBrands getCarBrands() {
        return carBrands;
    }

    public CarbrandsCarTypes carBrands(CarBrands carBrands) {
        this.carBrands = carBrands;
        return this;
    }

    public void setCarBrands(CarBrands carBrands) {
        this.carBrands = carBrands;
    }

    public CarTypes getCarTypes() {
        return carTypes;
    }

    public CarbrandsCarTypes carTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
        return this;
    }

    public void setCarTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarbrandsCarTypes carbrandsCarTypes = (CarbrandsCarTypes) o;
        if (carbrandsCarTypes.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carbrandsCarTypes.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarbrandsCarTypes{" +
            "id=" + getId() +
            "}";
    }
}
