package com.cartienda.carconfig.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Prices.
 */
@Entity
@Table(name = "prices")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Prices implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date_prices", nullable = false)
    private String datePrices;

    @NotNull
    @Column(name = "price", precision=10, scale=2, nullable = false)
    private BigDecimal price;

    @ManyToOne
    private SpecificsCar specificsCar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatePrices() {
        return datePrices;
    }

    public Prices datePrices(String datePrices) {
        this.datePrices = datePrices;
        return this;
    }

    public void setDatePrices(String datePrices) {
        this.datePrices = datePrices;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Prices price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public SpecificsCar getSpecificsCar() {
        return specificsCar;
    }

    public Prices specificsCar(SpecificsCar specificsCar) {
        this.specificsCar = specificsCar;
        return this;
    }

    public void setSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCar = specificsCar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Prices prices = (Prices) o;
        if (prices.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), prices.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Prices{" +
            "id=" + getId() +
            ", datePrices='" + getDatePrices() + "'" +
            ", price='" + getPrice() + "'" +
            "}";
    }
}
