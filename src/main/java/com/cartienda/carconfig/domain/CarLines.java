package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CarLines.
 */
@Entity
@Table(name = "car_lines")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CarLines implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "carLines")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CarVersions> carVersions = new HashSet<>();

    @OneToMany(mappedBy = "carLines")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpecificsCar> specificsCars = new HashSet<>();

    @ManyToOne
    private CarbrandsCarTypes carbrandsCarTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public CarLines description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CarVersions> getCarVersions() {
        return carVersions;
    }

    public CarLines carVersions(Set<CarVersions> carVersions) {
        this.carVersions = carVersions;
        return this;
    }

    public CarLines addCarVersions(CarVersions carVersions) {
        this.carVersions.add(carVersions);
        carVersions.setCarLines(this);
        return this;
    }

    public CarLines removeCarVersions(CarVersions carVersions) {
        this.carVersions.remove(carVersions);
        carVersions.setCarLines(null);
        return this;
    }

    public void setCarVersions(Set<CarVersions> carVersions) {
        this.carVersions = carVersions;
    }

    public Set<SpecificsCar> getSpecificsCars() {
        return specificsCars;
    }

    public CarLines specificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
        return this;
    }

    public CarLines addSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.add(specificsCar);
        specificsCar.setCarLines(this);
        return this;
    }

    public CarLines removeSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.remove(specificsCar);
        specificsCar.setCarLines(null);
        return this;
    }

    public void setSpecificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
    }

    public CarbrandsCarTypes getCarbrandsCarTypes() {
        return carbrandsCarTypes;
    }

    public CarLines carbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
        return this;
    }

    public void setCarbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarLines carLines = (CarLines) o;
        if (carLines.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carLines.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarLines{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
