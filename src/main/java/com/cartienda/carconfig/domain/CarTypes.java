package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CarTypes.
 */
@Entity
@Table(name = "car_types")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CarTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "carTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CartypesFields> carFields = new HashSet<>();

    @OneToMany(mappedBy = "carTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CarbrandsCarTypes> carbrandsCarTypes = new HashSet<>();

    @OneToMany(mappedBy = "carTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpecificsCar> specificsCars = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public CarTypes description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CartypesFields> getCarFields() {
        return carFields;
    }

    public CarTypes carFields(Set<CartypesFields> cartypesFields) {
        this.carFields = cartypesFields;
        return this;
    }

    public CarTypes addCarField(CartypesFields cartypesFields) {
        this.carFields.add(cartypesFields);
        cartypesFields.setCarTypes(this);
        return this;
    }

    public CarTypes removeCarField(CartypesFields cartypesFields) {
        this.carFields.remove(cartypesFields);
        cartypesFields.setCarTypes(null);
        return this;
    }

    public void setCarFields(Set<CartypesFields> cartypesFields) {
        this.carFields = cartypesFields;
    }

    public Set<CarbrandsCarTypes> getCarbrandsCarTypes() {
        return carbrandsCarTypes;
    }

    public CarTypes carbrandsCarTypes(Set<CarbrandsCarTypes> carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
        return this;
    }

    public CarTypes addCarbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes.add(carbrandsCarTypes);
        carbrandsCarTypes.setCarTypes(this);
        return this;
    }

    public CarTypes removeCarbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes.remove(carbrandsCarTypes);
        carbrandsCarTypes.setCarTypes(null);
        return this;
    }

    public void setCarbrandsCarTypes(Set<CarbrandsCarTypes> carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
    }

    public Set<SpecificsCar> getSpecificsCars() {
        return specificsCars;
    }

    public CarTypes specificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
        return this;
    }

    public CarTypes addSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.add(specificsCar);
        specificsCar.setCarTypes(this);
        return this;
    }

    public CarTypes removeSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.remove(specificsCar);
        specificsCar.setCarTypes(null);
        return this;
    }

    public void setSpecificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarTypes carTypes = (CarTypes) o;
        if (carTypes.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carTypes.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarTypes{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
