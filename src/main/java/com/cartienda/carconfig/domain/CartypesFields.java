package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CartypesFields.
 */
@Entity
@Table(name = "cartypes_fields")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CartypesFields implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "carTypesFields")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ListValues> listValues = new HashSet<>();

    @ManyToOne
    private Fields fields;

    @ManyToOne
    private CarTypes carTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ListValues> getListValues() {
        return listValues;
    }

    public CartypesFields listValues(Set<ListValues> listValues) {
        this.listValues = listValues;
        return this;
    }

    public CartypesFields addListValue(ListValues listValues) {
        this.listValues.add(listValues);
        listValues.setCarTypesFields(this);
        return this;
    }

    public CartypesFields removeListValue(ListValues listValues) {
        this.listValues.remove(listValues);
        listValues.setCarTypesFields(null);
        return this;
    }

    public void setListValues(Set<ListValues> listValues) {
        this.listValues = listValues;
    }

    public Fields getFields() {
        return fields;
    }

    public CartypesFields fields(Fields fields) {
        this.fields = fields;
        return this;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public CarTypes getCarTypes() {
        return carTypes;
    }

    public CartypesFields carTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
        return this;
    }

    public void setCarTypes(CarTypes carTypes) {
        this.carTypes = carTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CartypesFields cartypesFields = (CartypesFields) o;
        if (cartypesFields.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cartypesFields.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CartypesFields{" +
            "id=" + getId() +
            "}";
    }
}
