package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CarBrands.
 */
@Entity
@Table(name = "car_brands")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CarBrands implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "carBrands")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CarbrandsCarTypes> carbrandsCarTypes = new HashSet<>();

    @OneToMany(mappedBy = "carBrands")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpecificsCar> specificsCars = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public CarBrands description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CarbrandsCarTypes> getCarbrandsCarTypes() {
        return carbrandsCarTypes;
    }

    public CarBrands carbrandsCarTypes(Set<CarbrandsCarTypes> carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
        return this;
    }

    public CarBrands addCarbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes.add(carbrandsCarTypes);
        carbrandsCarTypes.setCarBrands(this);
        return this;
    }

    public CarBrands removeCarbrandsCarTypes(CarbrandsCarTypes carbrandsCarTypes) {
        this.carbrandsCarTypes.remove(carbrandsCarTypes);
        carbrandsCarTypes.setCarBrands(null);
        return this;
    }

    public void setCarbrandsCarTypes(Set<CarbrandsCarTypes> carbrandsCarTypes) {
        this.carbrandsCarTypes = carbrandsCarTypes;
    }

    public Set<SpecificsCar> getSpecificsCars() {
        return specificsCars;
    }

    public CarBrands specificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
        return this;
    }

    public CarBrands addSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.add(specificsCar);
        specificsCar.setCarBrands(this);
        return this;
    }

    public CarBrands removeSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.remove(specificsCar);
        specificsCar.setCarBrands(null);
        return this;
    }

    public void setSpecificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarBrands carBrands = (CarBrands) o;
        if (carBrands.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carBrands.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarBrands{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
