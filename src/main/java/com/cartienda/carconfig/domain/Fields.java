package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Fields.
 */
@Entity
@Table(name = "fields")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Fields implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @NotNull
    @Size(max = 255)
    @Column(name = "type_field", length = 255, nullable = false)
    private String typeField;

    @OneToMany(mappedBy = "fields")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CartypesFields> carFields = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Fields description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeField() {
        return typeField;
    }

    public Fields typeField(String typeField) {
        this.typeField = typeField;
        return this;
    }

    public void setTypeField(String typeField) {
        this.typeField = typeField;
    }

    public Set<CartypesFields> getCarFields() {
        return carFields;
    }

    public Fields carFields(Set<CartypesFields> cartypesFields) {
        this.carFields = cartypesFields;
        return this;
    }

    public Fields addCarField(CartypesFields cartypesFields) {
        this.carFields.add(cartypesFields);
        cartypesFields.setFields(this);
        return this;
    }

    public Fields removeCarField(CartypesFields cartypesFields) {
        this.carFields.remove(cartypesFields);
        cartypesFields.setFields(null);
        return this;
    }

    public void setCarFields(Set<CartypesFields> cartypesFields) {
        this.carFields = cartypesFields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fields fields = (Fields) o;
        if (fields.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fields.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Fields{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", typeField='" + getTypeField() + "'" +
            "}";
    }
}
