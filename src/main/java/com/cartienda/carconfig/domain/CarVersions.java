package com.cartienda.carconfig.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CarVersions.
 */
@Entity
@Table(name = "car_versions")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CarVersions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "carVersions")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpecificsCar> specificsCars = new HashSet<>();

    @ManyToOne
    private CarLines carLines;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public CarVersions description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<SpecificsCar> getSpecificsCars() {
        return specificsCars;
    }

    public CarVersions specificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
        return this;
    }

    public CarVersions addSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.add(specificsCar);
        specificsCar.setCarVersions(this);
        return this;
    }

    public CarVersions removeSpecificsCar(SpecificsCar specificsCar) {
        this.specificsCars.remove(specificsCar);
        specificsCar.setCarVersions(null);
        return this;
    }

    public void setSpecificsCars(Set<SpecificsCar> specificsCars) {
        this.specificsCars = specificsCars;
    }

    public CarLines getCarLines() {
        return carLines;
    }

    public CarVersions carLines(CarLines carLines) {
        this.carLines = carLines;
        return this;
    }

    public void setCarLines(CarLines carLines) {
        this.carLines = carLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarVersions carVersions = (CarVersions) o;
        if (carVersions.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carVersions.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CarVersions{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
