package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CarLines;
import com.cartienda.carconfig.repository.CarLinesRepository;
import com.cartienda.carconfig.service.CarLinesService;
import com.cartienda.carconfig.service.dto.CarLinesDTO;
import com.cartienda.carconfig.service.mapper.CarLinesMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarLinesResource REST controller.
 *
 * @see CarLinesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CarLinesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CarLinesRepository carLinesRepository;

    @Autowired
    private CarLinesMapper carLinesMapper;

    @Autowired
    private CarLinesService carLinesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarLinesMockMvc;

    private CarLines carLines;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarLinesResource carLinesResource = new CarLinesResource(carLinesService);
        this.restCarLinesMockMvc = MockMvcBuilders.standaloneSetup(carLinesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarLines createEntity(EntityManager em) {
        CarLines carLines = new CarLines()
            .description(DEFAULT_DESCRIPTION);
        return carLines;
    }

    @Before
    public void initTest() {
        carLines = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarLines() throws Exception {
        int databaseSizeBeforeCreate = carLinesRepository.findAll().size();

        // Create the CarLines
        CarLinesDTO carLinesDTO = carLinesMapper.toDto(carLines);
        restCarLinesMockMvc.perform(post("/api/car-lines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carLinesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarLines in the database
        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeCreate + 1);
        CarLines testCarLines = carLinesList.get(carLinesList.size() - 1);
        assertThat(testCarLines.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCarLinesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carLinesRepository.findAll().size();

        // Create the CarLines with an existing ID
        carLines.setId(1L);
        CarLinesDTO carLinesDTO = carLinesMapper.toDto(carLines);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarLinesMockMvc.perform(post("/api/car-lines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carLinesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = carLinesRepository.findAll().size();
        // set the field null
        carLines.setDescription(null);

        // Create the CarLines, which fails.
        CarLinesDTO carLinesDTO = carLinesMapper.toDto(carLines);

        restCarLinesMockMvc.perform(post("/api/car-lines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carLinesDTO)))
            .andExpect(status().isBadRequest());

        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarLines() throws Exception {
        // Initialize the database
        carLinesRepository.saveAndFlush(carLines);

        // Get all the carLinesList
        restCarLinesMockMvc.perform(get("/api/car-lines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carLines.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getCarLines() throws Exception {
        // Initialize the database
        carLinesRepository.saveAndFlush(carLines);

        // Get the carLines
        restCarLinesMockMvc.perform(get("/api/car-lines/{id}", carLines.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carLines.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarLines() throws Exception {
        // Get the carLines
        restCarLinesMockMvc.perform(get("/api/car-lines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarLines() throws Exception {
        // Initialize the database
        carLinesRepository.saveAndFlush(carLines);
        int databaseSizeBeforeUpdate = carLinesRepository.findAll().size();

        // Update the carLines
        CarLines updatedCarLines = carLinesRepository.findOne(carLines.getId());
        updatedCarLines
            .description(UPDATED_DESCRIPTION);
        CarLinesDTO carLinesDTO = carLinesMapper.toDto(updatedCarLines);

        restCarLinesMockMvc.perform(put("/api/car-lines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carLinesDTO)))
            .andExpect(status().isOk());

        // Validate the CarLines in the database
        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeUpdate);
        CarLines testCarLines = carLinesList.get(carLinesList.size() - 1);
        assertThat(testCarLines.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCarLines() throws Exception {
        int databaseSizeBeforeUpdate = carLinesRepository.findAll().size();

        // Create the CarLines
        CarLinesDTO carLinesDTO = carLinesMapper.toDto(carLines);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarLinesMockMvc.perform(put("/api/car-lines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carLinesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarLines in the database
        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarLines() throws Exception {
        // Initialize the database
        carLinesRepository.saveAndFlush(carLines);
        int databaseSizeBeforeDelete = carLinesRepository.findAll().size();

        // Get the carLines
        restCarLinesMockMvc.perform(delete("/api/car-lines/{id}", carLines.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CarLines> carLinesList = carLinesRepository.findAll();
        assertThat(carLinesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarLines.class);
        CarLines carLines1 = new CarLines();
        carLines1.setId(1L);
        CarLines carLines2 = new CarLines();
        carLines2.setId(carLines1.getId());
        assertThat(carLines1).isEqualTo(carLines2);
        carLines2.setId(2L);
        assertThat(carLines1).isNotEqualTo(carLines2);
        carLines1.setId(null);
        assertThat(carLines1).isNotEqualTo(carLines2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarLinesDTO.class);
        CarLinesDTO carLinesDTO1 = new CarLinesDTO();
        carLinesDTO1.setId(1L);
        CarLinesDTO carLinesDTO2 = new CarLinesDTO();
        assertThat(carLinesDTO1).isNotEqualTo(carLinesDTO2);
        carLinesDTO2.setId(carLinesDTO1.getId());
        assertThat(carLinesDTO1).isEqualTo(carLinesDTO2);
        carLinesDTO2.setId(2L);
        assertThat(carLinesDTO1).isNotEqualTo(carLinesDTO2);
        carLinesDTO1.setId(null);
        assertThat(carLinesDTO1).isNotEqualTo(carLinesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(carLinesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(carLinesMapper.fromId(null)).isNull();
    }
}
