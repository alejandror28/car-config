package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CarBrands;
import com.cartienda.carconfig.repository.CarBrandsRepository;
import com.cartienda.carconfig.service.CarBrandsService;
import com.cartienda.carconfig.service.dto.CarBrandsDTO;
import com.cartienda.carconfig.service.mapper.CarBrandsMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarBrandsResource REST controller.
 *
 * @see CarBrandsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CarBrandsResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CarBrandsRepository carBrandsRepository;

    @Autowired
    private CarBrandsMapper carBrandsMapper;

    @Autowired
    private CarBrandsService carBrandsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarBrandsMockMvc;

    private CarBrands carBrands;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarBrandsResource carBrandsResource = new CarBrandsResource(carBrandsService);
        this.restCarBrandsMockMvc = MockMvcBuilders.standaloneSetup(carBrandsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarBrands createEntity(EntityManager em) {
        CarBrands carBrands = new CarBrands()
            .description(DEFAULT_DESCRIPTION);
        return carBrands;
    }

    @Before
    public void initTest() {
        carBrands = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarBrands() throws Exception {
        int databaseSizeBeforeCreate = carBrandsRepository.findAll().size();

        // Create the CarBrands
        CarBrandsDTO carBrandsDTO = carBrandsMapper.toDto(carBrands);
        restCarBrandsMockMvc.perform(post("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrandsDTO)))
            .andExpect(status().isCreated());

        // Validate the CarBrands in the database
        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeCreate + 1);
        CarBrands testCarBrands = carBrandsList.get(carBrandsList.size() - 1);
        assertThat(testCarBrands.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCarBrandsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carBrandsRepository.findAll().size();

        // Create the CarBrands with an existing ID
        carBrands.setId(1L);
        CarBrandsDTO carBrandsDTO = carBrandsMapper.toDto(carBrands);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarBrandsMockMvc.perform(post("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrandsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = carBrandsRepository.findAll().size();
        // set the field null
        carBrands.setDescription(null);

        // Create the CarBrands, which fails.
        CarBrandsDTO carBrandsDTO = carBrandsMapper.toDto(carBrands);

        restCarBrandsMockMvc.perform(post("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrandsDTO)))
            .andExpect(status().isBadRequest());

        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarBrands() throws Exception {
        // Initialize the database
        carBrandsRepository.saveAndFlush(carBrands);

        // Get all the carBrandsList
        restCarBrandsMockMvc.perform(get("/api/car-brands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carBrands.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getCarBrands() throws Exception {
        // Initialize the database
        carBrandsRepository.saveAndFlush(carBrands);

        // Get the carBrands
        restCarBrandsMockMvc.perform(get("/api/car-brands/{id}", carBrands.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carBrands.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarBrands() throws Exception {
        // Get the carBrands
        restCarBrandsMockMvc.perform(get("/api/car-brands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarBrands() throws Exception {
        // Initialize the database
        carBrandsRepository.saveAndFlush(carBrands);
        int databaseSizeBeforeUpdate = carBrandsRepository.findAll().size();

        // Update the carBrands
        CarBrands updatedCarBrands = carBrandsRepository.findOne(carBrands.getId());
        updatedCarBrands
            .description(UPDATED_DESCRIPTION);
        CarBrandsDTO carBrandsDTO = carBrandsMapper.toDto(updatedCarBrands);

        restCarBrandsMockMvc.perform(put("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrandsDTO)))
            .andExpect(status().isOk());

        // Validate the CarBrands in the database
        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeUpdate);
        CarBrands testCarBrands = carBrandsList.get(carBrandsList.size() - 1);
        assertThat(testCarBrands.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCarBrands() throws Exception {
        int databaseSizeBeforeUpdate = carBrandsRepository.findAll().size();

        // Create the CarBrands
        CarBrandsDTO carBrandsDTO = carBrandsMapper.toDto(carBrands);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarBrandsMockMvc.perform(put("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrandsDTO)))
            .andExpect(status().isCreated());

        // Validate the CarBrands in the database
        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarBrands() throws Exception {
        // Initialize the database
        carBrandsRepository.saveAndFlush(carBrands);
        int databaseSizeBeforeDelete = carBrandsRepository.findAll().size();

        // Get the carBrands
        restCarBrandsMockMvc.perform(delete("/api/car-brands/{id}", carBrands.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CarBrands> carBrandsList = carBrandsRepository.findAll();
        assertThat(carBrandsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarBrands.class);
        CarBrands carBrands1 = new CarBrands();
        carBrands1.setId(1L);
        CarBrands carBrands2 = new CarBrands();
        carBrands2.setId(carBrands1.getId());
        assertThat(carBrands1).isEqualTo(carBrands2);
        carBrands2.setId(2L);
        assertThat(carBrands1).isNotEqualTo(carBrands2);
        carBrands1.setId(null);
        assertThat(carBrands1).isNotEqualTo(carBrands2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarBrandsDTO.class);
        CarBrandsDTO carBrandsDTO1 = new CarBrandsDTO();
        carBrandsDTO1.setId(1L);
        CarBrandsDTO carBrandsDTO2 = new CarBrandsDTO();
        assertThat(carBrandsDTO1).isNotEqualTo(carBrandsDTO2);
        carBrandsDTO2.setId(carBrandsDTO1.getId());
        assertThat(carBrandsDTO1).isEqualTo(carBrandsDTO2);
        carBrandsDTO2.setId(2L);
        assertThat(carBrandsDTO1).isNotEqualTo(carBrandsDTO2);
        carBrandsDTO1.setId(null);
        assertThat(carBrandsDTO1).isNotEqualTo(carBrandsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(carBrandsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(carBrandsMapper.fromId(null)).isNull();
    }
}
