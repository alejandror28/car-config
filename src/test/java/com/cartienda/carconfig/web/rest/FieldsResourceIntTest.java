package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.Fields;
import com.cartienda.carconfig.repository.FieldsRepository;
import com.cartienda.carconfig.service.FieldsService;
import com.cartienda.carconfig.service.dto.FieldsDTO;
import com.cartienda.carconfig.service.mapper.FieldsMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FieldsResource REST controller.
 *
 * @see FieldsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class FieldsResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_FIELD = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_FIELD = "BBBBBBBBBB";

    @Autowired
    private FieldsRepository fieldsRepository;

    @Autowired
    private FieldsMapper fieldsMapper;

    @Autowired
    private FieldsService fieldsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFieldsMockMvc;

    private Fields fields;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FieldsResource fieldsResource = new FieldsResource(fieldsService);
        this.restFieldsMockMvc = MockMvcBuilders.standaloneSetup(fieldsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fields createEntity(EntityManager em) {
        Fields fields = new Fields()
            .description(DEFAULT_DESCRIPTION)
            .typeField(DEFAULT_TYPE_FIELD);
        return fields;
    }

    @Before
    public void initTest() {
        fields = createEntity(em);
    }

    @Test
    @Transactional
    public void createFields() throws Exception {
        int databaseSizeBeforeCreate = fieldsRepository.findAll().size();

        // Create the Fields
        FieldsDTO fieldsDTO = fieldsMapper.toDto(fields);
        restFieldsMockMvc.perform(post("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isCreated());

        // Validate the Fields in the database
        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeCreate + 1);
        Fields testFields = fieldsList.get(fieldsList.size() - 1);
        assertThat(testFields.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFields.getTypeField()).isEqualTo(DEFAULT_TYPE_FIELD);
    }

    @Test
    @Transactional
    public void createFieldsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fieldsRepository.findAll().size();

        // Create the Fields with an existing ID
        fields.setId(1L);
        FieldsDTO fieldsDTO = fieldsMapper.toDto(fields);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFieldsMockMvc.perform(post("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = fieldsRepository.findAll().size();
        // set the field null
        fields.setDescription(null);

        // Create the Fields, which fails.
        FieldsDTO fieldsDTO = fieldsMapper.toDto(fields);

        restFieldsMockMvc.perform(post("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isBadRequest());

        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeFieldIsRequired() throws Exception {
        int databaseSizeBeforeTest = fieldsRepository.findAll().size();
        // set the field null
        fields.setTypeField(null);

        // Create the Fields, which fails.
        FieldsDTO fieldsDTO = fieldsMapper.toDto(fields);

        restFieldsMockMvc.perform(post("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isBadRequest());

        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFields() throws Exception {
        // Initialize the database
        fieldsRepository.saveAndFlush(fields);

        // Get all the fieldsList
        restFieldsMockMvc.perform(get("/api/fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fields.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].typeField").value(hasItem(DEFAULT_TYPE_FIELD.toString())));
    }

    @Test
    @Transactional
    public void getFields() throws Exception {
        // Initialize the database
        fieldsRepository.saveAndFlush(fields);

        // Get the fields
        restFieldsMockMvc.perform(get("/api/fields/{id}", fields.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fields.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.typeField").value(DEFAULT_TYPE_FIELD.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFields() throws Exception {
        // Get the fields
        restFieldsMockMvc.perform(get("/api/fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFields() throws Exception {
        // Initialize the database
        fieldsRepository.saveAndFlush(fields);
        int databaseSizeBeforeUpdate = fieldsRepository.findAll().size();

        // Update the fields
        Fields updatedFields = fieldsRepository.findOne(fields.getId());
        updatedFields
            .description(UPDATED_DESCRIPTION)
            .typeField(UPDATED_TYPE_FIELD);
        FieldsDTO fieldsDTO = fieldsMapper.toDto(updatedFields);

        restFieldsMockMvc.perform(put("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isOk());

        // Validate the Fields in the database
        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeUpdate);
        Fields testFields = fieldsList.get(fieldsList.size() - 1);
        assertThat(testFields.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFields.getTypeField()).isEqualTo(UPDATED_TYPE_FIELD);
    }

    @Test
    @Transactional
    public void updateNonExistingFields() throws Exception {
        int databaseSizeBeforeUpdate = fieldsRepository.findAll().size();

        // Create the Fields
        FieldsDTO fieldsDTO = fieldsMapper.toDto(fields);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFieldsMockMvc.perform(put("/api/fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fieldsDTO)))
            .andExpect(status().isCreated());

        // Validate the Fields in the database
        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFields() throws Exception {
        // Initialize the database
        fieldsRepository.saveAndFlush(fields);
        int databaseSizeBeforeDelete = fieldsRepository.findAll().size();

        // Get the fields
        restFieldsMockMvc.perform(delete("/api/fields/{id}", fields.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Fields> fieldsList = fieldsRepository.findAll();
        assertThat(fieldsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fields.class);
        Fields fields1 = new Fields();
        fields1.setId(1L);
        Fields fields2 = new Fields();
        fields2.setId(fields1.getId());
        assertThat(fields1).isEqualTo(fields2);
        fields2.setId(2L);
        assertThat(fields1).isNotEqualTo(fields2);
        fields1.setId(null);
        assertThat(fields1).isNotEqualTo(fields2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FieldsDTO.class);
        FieldsDTO fieldsDTO1 = new FieldsDTO();
        fieldsDTO1.setId(1L);
        FieldsDTO fieldsDTO2 = new FieldsDTO();
        assertThat(fieldsDTO1).isNotEqualTo(fieldsDTO2);
        fieldsDTO2.setId(fieldsDTO1.getId());
        assertThat(fieldsDTO1).isEqualTo(fieldsDTO2);
        fieldsDTO2.setId(2L);
        assertThat(fieldsDTO1).isNotEqualTo(fieldsDTO2);
        fieldsDTO1.setId(null);
        assertThat(fieldsDTO1).isNotEqualTo(fieldsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(fieldsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(fieldsMapper.fromId(null)).isNull();
    }
}
