package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CarVersions;
import com.cartienda.carconfig.repository.CarVersionsRepository;
import com.cartienda.carconfig.service.CarVersionsService;
import com.cartienda.carconfig.service.dto.CarVersionsDTO;
import com.cartienda.carconfig.service.mapper.CarVersionsMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarVersionsResource REST controller.
 *
 * @see CarVersionsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CarVersionsResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CarVersionsRepository carVersionsRepository;

    @Autowired
    private CarVersionsMapper carVersionsMapper;

    @Autowired
    private CarVersionsService carVersionsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarVersionsMockMvc;

    private CarVersions carVersions;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarVersionsResource carVersionsResource = new CarVersionsResource(carVersionsService);
        this.restCarVersionsMockMvc = MockMvcBuilders.standaloneSetup(carVersionsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarVersions createEntity(EntityManager em) {
        CarVersions carVersions = new CarVersions()
            .description(DEFAULT_DESCRIPTION);
        return carVersions;
    }

    @Before
    public void initTest() {
        carVersions = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarVersions() throws Exception {
        int databaseSizeBeforeCreate = carVersionsRepository.findAll().size();

        // Create the CarVersions
        CarVersionsDTO carVersionsDTO = carVersionsMapper.toDto(carVersions);
        restCarVersionsMockMvc.perform(post("/api/car-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carVersionsDTO)))
            .andExpect(status().isCreated());

        // Validate the CarVersions in the database
        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeCreate + 1);
        CarVersions testCarVersions = carVersionsList.get(carVersionsList.size() - 1);
        assertThat(testCarVersions.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCarVersionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carVersionsRepository.findAll().size();

        // Create the CarVersions with an existing ID
        carVersions.setId(1L);
        CarVersionsDTO carVersionsDTO = carVersionsMapper.toDto(carVersions);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarVersionsMockMvc.perform(post("/api/car-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carVersionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = carVersionsRepository.findAll().size();
        // set the field null
        carVersions.setDescription(null);

        // Create the CarVersions, which fails.
        CarVersionsDTO carVersionsDTO = carVersionsMapper.toDto(carVersions);

        restCarVersionsMockMvc.perform(post("/api/car-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carVersionsDTO)))
            .andExpect(status().isBadRequest());

        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarVersions() throws Exception {
        // Initialize the database
        carVersionsRepository.saveAndFlush(carVersions);

        // Get all the carVersionsList
        restCarVersionsMockMvc.perform(get("/api/car-versions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carVersions.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getCarVersions() throws Exception {
        // Initialize the database
        carVersionsRepository.saveAndFlush(carVersions);

        // Get the carVersions
        restCarVersionsMockMvc.perform(get("/api/car-versions/{id}", carVersions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carVersions.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarVersions() throws Exception {
        // Get the carVersions
        restCarVersionsMockMvc.perform(get("/api/car-versions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarVersions() throws Exception {
        // Initialize the database
        carVersionsRepository.saveAndFlush(carVersions);
        int databaseSizeBeforeUpdate = carVersionsRepository.findAll().size();

        // Update the carVersions
        CarVersions updatedCarVersions = carVersionsRepository.findOne(carVersions.getId());
        updatedCarVersions
            .description(UPDATED_DESCRIPTION);
        CarVersionsDTO carVersionsDTO = carVersionsMapper.toDto(updatedCarVersions);

        restCarVersionsMockMvc.perform(put("/api/car-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carVersionsDTO)))
            .andExpect(status().isOk());

        // Validate the CarVersions in the database
        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeUpdate);
        CarVersions testCarVersions = carVersionsList.get(carVersionsList.size() - 1);
        assertThat(testCarVersions.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCarVersions() throws Exception {
        int databaseSizeBeforeUpdate = carVersionsRepository.findAll().size();

        // Create the CarVersions
        CarVersionsDTO carVersionsDTO = carVersionsMapper.toDto(carVersions);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarVersionsMockMvc.perform(put("/api/car-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carVersionsDTO)))
            .andExpect(status().isCreated());

        // Validate the CarVersions in the database
        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarVersions() throws Exception {
        // Initialize the database
        carVersionsRepository.saveAndFlush(carVersions);
        int databaseSizeBeforeDelete = carVersionsRepository.findAll().size();

        // Get the carVersions
        restCarVersionsMockMvc.perform(delete("/api/car-versions/{id}", carVersions.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CarVersions> carVersionsList = carVersionsRepository.findAll();
        assertThat(carVersionsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarVersions.class);
        CarVersions carVersions1 = new CarVersions();
        carVersions1.setId(1L);
        CarVersions carVersions2 = new CarVersions();
        carVersions2.setId(carVersions1.getId());
        assertThat(carVersions1).isEqualTo(carVersions2);
        carVersions2.setId(2L);
        assertThat(carVersions1).isNotEqualTo(carVersions2);
        carVersions1.setId(null);
        assertThat(carVersions1).isNotEqualTo(carVersions2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarVersionsDTO.class);
        CarVersionsDTO carVersionsDTO1 = new CarVersionsDTO();
        carVersionsDTO1.setId(1L);
        CarVersionsDTO carVersionsDTO2 = new CarVersionsDTO();
        assertThat(carVersionsDTO1).isNotEqualTo(carVersionsDTO2);
        carVersionsDTO2.setId(carVersionsDTO1.getId());
        assertThat(carVersionsDTO1).isEqualTo(carVersionsDTO2);
        carVersionsDTO2.setId(2L);
        assertThat(carVersionsDTO1).isNotEqualTo(carVersionsDTO2);
        carVersionsDTO1.setId(null);
        assertThat(carVersionsDTO1).isNotEqualTo(carVersionsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(carVersionsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(carVersionsMapper.fromId(null)).isNull();
    }
}
