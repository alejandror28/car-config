package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CarbrandsCarTypes;
import com.cartienda.carconfig.repository.CarbrandsCarTypesRepository;
import com.cartienda.carconfig.service.CarbrandsCarTypesService;
import com.cartienda.carconfig.service.dto.CarbrandsCarTypesDTO;
import com.cartienda.carconfig.service.mapper.CarbrandsCarTypesMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarbrandsCarTypesResource REST controller.
 *
 * @see CarbrandsCarTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CarbrandsCarTypesResourceIntTest {

    @Autowired
    private CarbrandsCarTypesRepository carbrandsCarTypesRepository;

    @Autowired
    private CarbrandsCarTypesMapper carbrandsCarTypesMapper;

    @Autowired
    private CarbrandsCarTypesService carbrandsCarTypesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarbrandsCarTypesMockMvc;

    private CarbrandsCarTypes carbrandsCarTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarbrandsCarTypesResource carbrandsCarTypesResource = new CarbrandsCarTypesResource(carbrandsCarTypesService);
        this.restCarbrandsCarTypesMockMvc = MockMvcBuilders.standaloneSetup(carbrandsCarTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarbrandsCarTypes createEntity(EntityManager em) {
        CarbrandsCarTypes carbrandsCarTypes = new CarbrandsCarTypes();
        return carbrandsCarTypes;
    }

    @Before
    public void initTest() {
        carbrandsCarTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarbrandsCarTypes() throws Exception {
        int databaseSizeBeforeCreate = carbrandsCarTypesRepository.findAll().size();

        // Create the CarbrandsCarTypes
        CarbrandsCarTypesDTO carbrandsCarTypesDTO = carbrandsCarTypesMapper.toDto(carbrandsCarTypes);
        restCarbrandsCarTypesMockMvc.perform(post("/api/carbrands-car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carbrandsCarTypesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarbrandsCarTypes in the database
        List<CarbrandsCarTypes> carbrandsCarTypesList = carbrandsCarTypesRepository.findAll();
        assertThat(carbrandsCarTypesList).hasSize(databaseSizeBeforeCreate + 1);
        CarbrandsCarTypes testCarbrandsCarTypes = carbrandsCarTypesList.get(carbrandsCarTypesList.size() - 1);
    }

    @Test
    @Transactional
    public void createCarbrandsCarTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carbrandsCarTypesRepository.findAll().size();

        // Create the CarbrandsCarTypes with an existing ID
        carbrandsCarTypes.setId(1L);
        CarbrandsCarTypesDTO carbrandsCarTypesDTO = carbrandsCarTypesMapper.toDto(carbrandsCarTypes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarbrandsCarTypesMockMvc.perform(post("/api/carbrands-car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carbrandsCarTypesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarbrandsCarTypes> carbrandsCarTypesList = carbrandsCarTypesRepository.findAll();
        assertThat(carbrandsCarTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCarbrandsCarTypes() throws Exception {
        // Initialize the database
        carbrandsCarTypesRepository.saveAndFlush(carbrandsCarTypes);

        // Get all the carbrandsCarTypesList
        restCarbrandsCarTypesMockMvc.perform(get("/api/carbrands-car-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carbrandsCarTypes.getId().intValue())));
    }

    @Test
    @Transactional
    public void getCarbrandsCarTypes() throws Exception {
        // Initialize the database
        carbrandsCarTypesRepository.saveAndFlush(carbrandsCarTypes);

        // Get the carbrandsCarTypes
        restCarbrandsCarTypesMockMvc.perform(get("/api/carbrands-car-types/{id}", carbrandsCarTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carbrandsCarTypes.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCarbrandsCarTypes() throws Exception {
        // Get the carbrandsCarTypes
        restCarbrandsCarTypesMockMvc.perform(get("/api/carbrands-car-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarbrandsCarTypes() throws Exception {
        // Initialize the database
        carbrandsCarTypesRepository.saveAndFlush(carbrandsCarTypes);
        int databaseSizeBeforeUpdate = carbrandsCarTypesRepository.findAll().size();

        // Update the carbrandsCarTypes
        CarbrandsCarTypes updatedCarbrandsCarTypes = carbrandsCarTypesRepository.findOne(carbrandsCarTypes.getId());
        CarbrandsCarTypesDTO carbrandsCarTypesDTO = carbrandsCarTypesMapper.toDto(updatedCarbrandsCarTypes);

        restCarbrandsCarTypesMockMvc.perform(put("/api/carbrands-car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carbrandsCarTypesDTO)))
            .andExpect(status().isOk());

        // Validate the CarbrandsCarTypes in the database
        List<CarbrandsCarTypes> carbrandsCarTypesList = carbrandsCarTypesRepository.findAll();
        assertThat(carbrandsCarTypesList).hasSize(databaseSizeBeforeUpdate);
        CarbrandsCarTypes testCarbrandsCarTypes = carbrandsCarTypesList.get(carbrandsCarTypesList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCarbrandsCarTypes() throws Exception {
        int databaseSizeBeforeUpdate = carbrandsCarTypesRepository.findAll().size();

        // Create the CarbrandsCarTypes
        CarbrandsCarTypesDTO carbrandsCarTypesDTO = carbrandsCarTypesMapper.toDto(carbrandsCarTypes);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarbrandsCarTypesMockMvc.perform(put("/api/carbrands-car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carbrandsCarTypesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarbrandsCarTypes in the database
        List<CarbrandsCarTypes> carbrandsCarTypesList = carbrandsCarTypesRepository.findAll();
        assertThat(carbrandsCarTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarbrandsCarTypes() throws Exception {
        // Initialize the database
        carbrandsCarTypesRepository.saveAndFlush(carbrandsCarTypes);
        int databaseSizeBeforeDelete = carbrandsCarTypesRepository.findAll().size();

        // Get the carbrandsCarTypes
        restCarbrandsCarTypesMockMvc.perform(delete("/api/carbrands-car-types/{id}", carbrandsCarTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CarbrandsCarTypes> carbrandsCarTypesList = carbrandsCarTypesRepository.findAll();
        assertThat(carbrandsCarTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarbrandsCarTypes.class);
        CarbrandsCarTypes carbrandsCarTypes1 = new CarbrandsCarTypes();
        carbrandsCarTypes1.setId(1L);
        CarbrandsCarTypes carbrandsCarTypes2 = new CarbrandsCarTypes();
        carbrandsCarTypes2.setId(carbrandsCarTypes1.getId());
        assertThat(carbrandsCarTypes1).isEqualTo(carbrandsCarTypes2);
        carbrandsCarTypes2.setId(2L);
        assertThat(carbrandsCarTypes1).isNotEqualTo(carbrandsCarTypes2);
        carbrandsCarTypes1.setId(null);
        assertThat(carbrandsCarTypes1).isNotEqualTo(carbrandsCarTypes2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarbrandsCarTypesDTO.class);
        CarbrandsCarTypesDTO carbrandsCarTypesDTO1 = new CarbrandsCarTypesDTO();
        carbrandsCarTypesDTO1.setId(1L);
        CarbrandsCarTypesDTO carbrandsCarTypesDTO2 = new CarbrandsCarTypesDTO();
        assertThat(carbrandsCarTypesDTO1).isNotEqualTo(carbrandsCarTypesDTO2);
        carbrandsCarTypesDTO2.setId(carbrandsCarTypesDTO1.getId());
        assertThat(carbrandsCarTypesDTO1).isEqualTo(carbrandsCarTypesDTO2);
        carbrandsCarTypesDTO2.setId(2L);
        assertThat(carbrandsCarTypesDTO1).isNotEqualTo(carbrandsCarTypesDTO2);
        carbrandsCarTypesDTO1.setId(null);
        assertThat(carbrandsCarTypesDTO1).isNotEqualTo(carbrandsCarTypesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(carbrandsCarTypesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(carbrandsCarTypesMapper.fromId(null)).isNull();
    }
}
