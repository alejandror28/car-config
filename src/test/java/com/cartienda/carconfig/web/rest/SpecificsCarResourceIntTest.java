package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.SpecificsCar;
import com.cartienda.carconfig.repository.SpecificsCarRepository;
import com.cartienda.carconfig.service.SpecificsCarService;
import com.cartienda.carconfig.service.dto.SpecificsCarDTO;
import com.cartienda.carconfig.service.mapper.SpecificsCarMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SpecificsCarResource REST controller.
 *
 * @see SpecificsCarResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class SpecificsCarResourceIntTest {

    @Autowired
    private SpecificsCarRepository specificsCarRepository;

    @Autowired
    private SpecificsCarMapper specificsCarMapper;

    @Autowired
    private SpecificsCarService specificsCarService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSpecificsCarMockMvc;

    private SpecificsCar specificsCar;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SpecificsCarResource specificsCarResource = new SpecificsCarResource(specificsCarService);
        this.restSpecificsCarMockMvc = MockMvcBuilders.standaloneSetup(specificsCarResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpecificsCar createEntity(EntityManager em) {
        SpecificsCar specificsCar = new SpecificsCar();
        return specificsCar;
    }

    @Before
    public void initTest() {
        specificsCar = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpecificsCar() throws Exception {
        int databaseSizeBeforeCreate = specificsCarRepository.findAll().size();

        // Create the SpecificsCar
        SpecificsCarDTO specificsCarDTO = specificsCarMapper.toDto(specificsCar);
        restSpecificsCarMockMvc.perform(post("/api/specifics-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specificsCarDTO)))
            .andExpect(status().isCreated());

        // Validate the SpecificsCar in the database
        List<SpecificsCar> specificsCarList = specificsCarRepository.findAll();
        assertThat(specificsCarList).hasSize(databaseSizeBeforeCreate + 1);
        SpecificsCar testSpecificsCar = specificsCarList.get(specificsCarList.size() - 1);
    }

    @Test
    @Transactional
    public void createSpecificsCarWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = specificsCarRepository.findAll().size();

        // Create the SpecificsCar with an existing ID
        specificsCar.setId(1L);
        SpecificsCarDTO specificsCarDTO = specificsCarMapper.toDto(specificsCar);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpecificsCarMockMvc.perform(post("/api/specifics-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specificsCarDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SpecificsCar> specificsCarList = specificsCarRepository.findAll();
        assertThat(specificsCarList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSpecificsCars() throws Exception {
        // Initialize the database
        specificsCarRepository.saveAndFlush(specificsCar);

        // Get all the specificsCarList
        restSpecificsCarMockMvc.perform(get("/api/specifics-cars?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specificsCar.getId().intValue())));
    }

    @Test
    @Transactional
    public void getSpecificsCar() throws Exception {
        // Initialize the database
        specificsCarRepository.saveAndFlush(specificsCar);

        // Get the specificsCar
        restSpecificsCarMockMvc.perform(get("/api/specifics-cars/{id}", specificsCar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(specificsCar.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSpecificsCar() throws Exception {
        // Get the specificsCar
        restSpecificsCarMockMvc.perform(get("/api/specifics-cars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpecificsCar() throws Exception {
        // Initialize the database
        specificsCarRepository.saveAndFlush(specificsCar);
        int databaseSizeBeforeUpdate = specificsCarRepository.findAll().size();

        // Update the specificsCar
        SpecificsCar updatedSpecificsCar = specificsCarRepository.findOne(specificsCar.getId());
        SpecificsCarDTO specificsCarDTO = specificsCarMapper.toDto(updatedSpecificsCar);

        restSpecificsCarMockMvc.perform(put("/api/specifics-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specificsCarDTO)))
            .andExpect(status().isOk());

        // Validate the SpecificsCar in the database
        List<SpecificsCar> specificsCarList = specificsCarRepository.findAll();
        assertThat(specificsCarList).hasSize(databaseSizeBeforeUpdate);
        SpecificsCar testSpecificsCar = specificsCarList.get(specificsCarList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSpecificsCar() throws Exception {
        int databaseSizeBeforeUpdate = specificsCarRepository.findAll().size();

        // Create the SpecificsCar
        SpecificsCarDTO specificsCarDTO = specificsCarMapper.toDto(specificsCar);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSpecificsCarMockMvc.perform(put("/api/specifics-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specificsCarDTO)))
            .andExpect(status().isCreated());

        // Validate the SpecificsCar in the database
        List<SpecificsCar> specificsCarList = specificsCarRepository.findAll();
        assertThat(specificsCarList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSpecificsCar() throws Exception {
        // Initialize the database
        specificsCarRepository.saveAndFlush(specificsCar);
        int databaseSizeBeforeDelete = specificsCarRepository.findAll().size();

        // Get the specificsCar
        restSpecificsCarMockMvc.perform(delete("/api/specifics-cars/{id}", specificsCar.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SpecificsCar> specificsCarList = specificsCarRepository.findAll();
        assertThat(specificsCarList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpecificsCar.class);
        SpecificsCar specificsCar1 = new SpecificsCar();
        specificsCar1.setId(1L);
        SpecificsCar specificsCar2 = new SpecificsCar();
        specificsCar2.setId(specificsCar1.getId());
        assertThat(specificsCar1).isEqualTo(specificsCar2);
        specificsCar2.setId(2L);
        assertThat(specificsCar1).isNotEqualTo(specificsCar2);
        specificsCar1.setId(null);
        assertThat(specificsCar1).isNotEqualTo(specificsCar2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpecificsCarDTO.class);
        SpecificsCarDTO specificsCarDTO1 = new SpecificsCarDTO();
        specificsCarDTO1.setId(1L);
        SpecificsCarDTO specificsCarDTO2 = new SpecificsCarDTO();
        assertThat(specificsCarDTO1).isNotEqualTo(specificsCarDTO2);
        specificsCarDTO2.setId(specificsCarDTO1.getId());
        assertThat(specificsCarDTO1).isEqualTo(specificsCarDTO2);
        specificsCarDTO2.setId(2L);
        assertThat(specificsCarDTO1).isNotEqualTo(specificsCarDTO2);
        specificsCarDTO1.setId(null);
        assertThat(specificsCarDTO1).isNotEqualTo(specificsCarDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(specificsCarMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(specificsCarMapper.fromId(null)).isNull();
    }
}
