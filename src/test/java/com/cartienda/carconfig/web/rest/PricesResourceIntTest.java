package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.Prices;
import com.cartienda.carconfig.repository.PricesRepository;
import com.cartienda.carconfig.service.PricesService;
import com.cartienda.carconfig.service.dto.PricesDTO;
import com.cartienda.carconfig.service.mapper.PricesMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PricesResource REST controller.
 *
 * @see PricesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class PricesResourceIntTest {

    private static final String DEFAULT_DATE_PRICES = "AAAAAAAAAA";
    private static final String UPDATED_DATE_PRICES = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    @Autowired
    private PricesRepository pricesRepository;

    @Autowired
    private PricesMapper pricesMapper;

    @Autowired
    private PricesService pricesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPricesMockMvc;

    private Prices prices;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PricesResource pricesResource = new PricesResource(pricesService);
        this.restPricesMockMvc = MockMvcBuilders.standaloneSetup(pricesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prices createEntity(EntityManager em) {
        Prices prices = new Prices()
            .datePrices(DEFAULT_DATE_PRICES)
            .price(DEFAULT_PRICE);
        return prices;
    }

    @Before
    public void initTest() {
        prices = createEntity(em);
    }

    @Test
    @Transactional
    public void createPrices() throws Exception {
        int databaseSizeBeforeCreate = pricesRepository.findAll().size();

        // Create the Prices
        PricesDTO pricesDTO = pricesMapper.toDto(prices);
        restPricesMockMvc.perform(post("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isCreated());

        // Validate the Prices in the database
        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeCreate + 1);
        Prices testPrices = pricesList.get(pricesList.size() - 1);
        assertThat(testPrices.getDatePrices()).isEqualTo(DEFAULT_DATE_PRICES);
        assertThat(testPrices.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createPricesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pricesRepository.findAll().size();

        // Create the Prices with an existing ID
        prices.setId(1L);
        PricesDTO pricesDTO = pricesMapper.toDto(prices);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPricesMockMvc.perform(post("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDatePricesIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricesRepository.findAll().size();
        // set the field null
        prices.setDatePrices(null);

        // Create the Prices, which fails.
        PricesDTO pricesDTO = pricesMapper.toDto(prices);

        restPricesMockMvc.perform(post("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isBadRequest());

        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricesRepository.findAll().size();
        // set the field null
        prices.setPrice(null);

        // Create the Prices, which fails.
        PricesDTO pricesDTO = pricesMapper.toDto(prices);

        restPricesMockMvc.perform(post("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isBadRequest());

        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPrices() throws Exception {
        // Initialize the database
        pricesRepository.saveAndFlush(prices);

        // Get all the pricesList
        restPricesMockMvc.perform(get("/api/prices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prices.getId().intValue())))
            .andExpect(jsonPath("$.[*].datePrices").value(hasItem(DEFAULT_DATE_PRICES.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getPrices() throws Exception {
        // Initialize the database
        pricesRepository.saveAndFlush(prices);

        // Get the prices
        restPricesMockMvc.perform(get("/api/prices/{id}", prices.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(prices.getId().intValue()))
            .andExpect(jsonPath("$.datePrices").value(DEFAULT_DATE_PRICES.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPrices() throws Exception {
        // Get the prices
        restPricesMockMvc.perform(get("/api/prices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePrices() throws Exception {
        // Initialize the database
        pricesRepository.saveAndFlush(prices);
        int databaseSizeBeforeUpdate = pricesRepository.findAll().size();

        // Update the prices
        Prices updatedPrices = pricesRepository.findOne(prices.getId());
        updatedPrices
            .datePrices(UPDATED_DATE_PRICES)
            .price(UPDATED_PRICE);
        PricesDTO pricesDTO = pricesMapper.toDto(updatedPrices);

        restPricesMockMvc.perform(put("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isOk());

        // Validate the Prices in the database
        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeUpdate);
        Prices testPrices = pricesList.get(pricesList.size() - 1);
        assertThat(testPrices.getDatePrices()).isEqualTo(UPDATED_DATE_PRICES);
        assertThat(testPrices.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingPrices() throws Exception {
        int databaseSizeBeforeUpdate = pricesRepository.findAll().size();

        // Create the Prices
        PricesDTO pricesDTO = pricesMapper.toDto(prices);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPricesMockMvc.perform(put("/api/prices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricesDTO)))
            .andExpect(status().isCreated());

        // Validate the Prices in the database
        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePrices() throws Exception {
        // Initialize the database
        pricesRepository.saveAndFlush(prices);
        int databaseSizeBeforeDelete = pricesRepository.findAll().size();

        // Get the prices
        restPricesMockMvc.perform(delete("/api/prices/{id}", prices.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Prices> pricesList = pricesRepository.findAll();
        assertThat(pricesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Prices.class);
        Prices prices1 = new Prices();
        prices1.setId(1L);
        Prices prices2 = new Prices();
        prices2.setId(prices1.getId());
        assertThat(prices1).isEqualTo(prices2);
        prices2.setId(2L);
        assertThat(prices1).isNotEqualTo(prices2);
        prices1.setId(null);
        assertThat(prices1).isNotEqualTo(prices2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PricesDTO.class);
        PricesDTO pricesDTO1 = new PricesDTO();
        pricesDTO1.setId(1L);
        PricesDTO pricesDTO2 = new PricesDTO();
        assertThat(pricesDTO1).isNotEqualTo(pricesDTO2);
        pricesDTO2.setId(pricesDTO1.getId());
        assertThat(pricesDTO1).isEqualTo(pricesDTO2);
        pricesDTO2.setId(2L);
        assertThat(pricesDTO1).isNotEqualTo(pricesDTO2);
        pricesDTO1.setId(null);
        assertThat(pricesDTO1).isNotEqualTo(pricesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pricesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pricesMapper.fromId(null)).isNull();
    }
}
