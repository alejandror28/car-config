package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.ListValues;
import com.cartienda.carconfig.repository.ListValuesRepository;
import com.cartienda.carconfig.service.ListValuesService;
import com.cartienda.carconfig.service.dto.ListValuesDTO;
import com.cartienda.carconfig.service.mapper.ListValuesMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ListValuesResource REST controller.
 *
 * @see ListValuesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class ListValuesResourceIntTest {

    private static final String DEFAULT_LIST_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_LIST_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private ListValuesRepository listValuesRepository;

    @Autowired
    private ListValuesMapper listValuesMapper;

    @Autowired
    private ListValuesService listValuesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restListValuesMockMvc;

    private ListValues listValues;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ListValuesResource listValuesResource = new ListValuesResource(listValuesService);
        this.restListValuesMockMvc = MockMvcBuilders.standaloneSetup(listValuesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListValues createEntity(EntityManager em) {
        ListValues listValues = new ListValues()
            .listType(DEFAULT_LIST_TYPE)
            .value(DEFAULT_VALUE);
        return listValues;
    }

    @Before
    public void initTest() {
        listValues = createEntity(em);
    }

    @Test
    @Transactional
    public void createListValues() throws Exception {
        int databaseSizeBeforeCreate = listValuesRepository.findAll().size();

        // Create the ListValues
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(listValues);
        restListValuesMockMvc.perform(post("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isCreated());

        // Validate the ListValues in the database
        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeCreate + 1);
        ListValues testListValues = listValuesList.get(listValuesList.size() - 1);
        assertThat(testListValues.getListType()).isEqualTo(DEFAULT_LIST_TYPE);
        assertThat(testListValues.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createListValuesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listValuesRepository.findAll().size();

        // Create the ListValues with an existing ID
        listValues.setId(1L);
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(listValues);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListValuesMockMvc.perform(post("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkListTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = listValuesRepository.findAll().size();
        // set the field null
        listValues.setListType(null);

        // Create the ListValues, which fails.
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(listValues);

        restListValuesMockMvc.perform(post("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isBadRequest());

        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = listValuesRepository.findAll().size();
        // set the field null
        listValues.setValue(null);

        // Create the ListValues, which fails.
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(listValues);

        restListValuesMockMvc.perform(post("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isBadRequest());

        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllListValues() throws Exception {
        // Initialize the database
        listValuesRepository.saveAndFlush(listValues);

        // Get all the listValuesList
        restListValuesMockMvc.perform(get("/api/list-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listValues.getId().intValue())))
            .andExpect(jsonPath("$.[*].listType").value(hasItem(DEFAULT_LIST_TYPE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getListValues() throws Exception {
        // Initialize the database
        listValuesRepository.saveAndFlush(listValues);

        // Get the listValues
        restListValuesMockMvc.perform(get("/api/list-values/{id}", listValues.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(listValues.getId().intValue()))
            .andExpect(jsonPath("$.listType").value(DEFAULT_LIST_TYPE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingListValues() throws Exception {
        // Get the listValues
        restListValuesMockMvc.perform(get("/api/list-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListValues() throws Exception {
        // Initialize the database
        listValuesRepository.saveAndFlush(listValues);
        int databaseSizeBeforeUpdate = listValuesRepository.findAll().size();

        // Update the listValues
        ListValues updatedListValues = listValuesRepository.findOne(listValues.getId());
        updatedListValues
            .listType(UPDATED_LIST_TYPE)
            .value(UPDATED_VALUE);
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(updatedListValues);

        restListValuesMockMvc.perform(put("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isOk());

        // Validate the ListValues in the database
        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeUpdate);
        ListValues testListValues = listValuesList.get(listValuesList.size() - 1);
        assertThat(testListValues.getListType()).isEqualTo(UPDATED_LIST_TYPE);
        assertThat(testListValues.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingListValues() throws Exception {
        int databaseSizeBeforeUpdate = listValuesRepository.findAll().size();

        // Create the ListValues
        ListValuesDTO listValuesDTO = listValuesMapper.toDto(listValues);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restListValuesMockMvc.perform(put("/api/list-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listValuesDTO)))
            .andExpect(status().isCreated());

        // Validate the ListValues in the database
        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteListValues() throws Exception {
        // Initialize the database
        listValuesRepository.saveAndFlush(listValues);
        int databaseSizeBeforeDelete = listValuesRepository.findAll().size();

        // Get the listValues
        restListValuesMockMvc.perform(delete("/api/list-values/{id}", listValues.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ListValues> listValuesList = listValuesRepository.findAll();
        assertThat(listValuesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListValues.class);
        ListValues listValues1 = new ListValues();
        listValues1.setId(1L);
        ListValues listValues2 = new ListValues();
        listValues2.setId(listValues1.getId());
        assertThat(listValues1).isEqualTo(listValues2);
        listValues2.setId(2L);
        assertThat(listValues1).isNotEqualTo(listValues2);
        listValues1.setId(null);
        assertThat(listValues1).isNotEqualTo(listValues2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListValuesDTO.class);
        ListValuesDTO listValuesDTO1 = new ListValuesDTO();
        listValuesDTO1.setId(1L);
        ListValuesDTO listValuesDTO2 = new ListValuesDTO();
        assertThat(listValuesDTO1).isNotEqualTo(listValuesDTO2);
        listValuesDTO2.setId(listValuesDTO1.getId());
        assertThat(listValuesDTO1).isEqualTo(listValuesDTO2);
        listValuesDTO2.setId(2L);
        assertThat(listValuesDTO1).isNotEqualTo(listValuesDTO2);
        listValuesDTO1.setId(null);
        assertThat(listValuesDTO1).isNotEqualTo(listValuesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(listValuesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(listValuesMapper.fromId(null)).isNull();
    }
}
