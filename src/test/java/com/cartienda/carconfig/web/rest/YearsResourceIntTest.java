package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.Years;
import com.cartienda.carconfig.repository.YearsRepository;
import com.cartienda.carconfig.service.YearsService;
import com.cartienda.carconfig.service.dto.YearsDTO;
import com.cartienda.carconfig.service.mapper.YearsMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the YearsResource REST controller.
 *
 * @see YearsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class YearsResourceIntTest {

    private static final String DEFAULT_YEAR = "AAAAAAAAAA";
    private static final String UPDATED_YEAR = "BBBBBBBBBB";

    @Autowired
    private YearsRepository yearsRepository;

    @Autowired
    private YearsMapper yearsMapper;

    @Autowired
    private YearsService yearsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restYearsMockMvc;

    private Years years;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        YearsResource yearsResource = new YearsResource(yearsService);
        this.restYearsMockMvc = MockMvcBuilders.standaloneSetup(yearsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Years createEntity(EntityManager em) {
        Years years = new Years()
            .year(DEFAULT_YEAR);
        return years;
    }

    @Before
    public void initTest() {
        years = createEntity(em);
    }

    @Test
    @Transactional
    public void createYears() throws Exception {
        int databaseSizeBeforeCreate = yearsRepository.findAll().size();

        // Create the Years
        YearsDTO yearsDTO = yearsMapper.toDto(years);
        restYearsMockMvc.perform(post("/api/years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yearsDTO)))
            .andExpect(status().isCreated());

        // Validate the Years in the database
        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeCreate + 1);
        Years testYears = yearsList.get(yearsList.size() - 1);
        assertThat(testYears.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    public void createYearsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = yearsRepository.findAll().size();

        // Create the Years with an existing ID
        years.setId(1L);
        YearsDTO yearsDTO = yearsMapper.toDto(years);

        // An entity with an existing ID cannot be created, so this API call must fail
        restYearsMockMvc.perform(post("/api/years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yearsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkYearIsRequired() throws Exception {
        int databaseSizeBeforeTest = yearsRepository.findAll().size();
        // set the field null
        years.setYear(null);

        // Create the Years, which fails.
        YearsDTO yearsDTO = yearsMapper.toDto(years);

        restYearsMockMvc.perform(post("/api/years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yearsDTO)))
            .andExpect(status().isBadRequest());

        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllYears() throws Exception {
        // Initialize the database
        yearsRepository.saveAndFlush(years);

        // Get all the yearsList
        restYearsMockMvc.perform(get("/api/years?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(years.getId().intValue())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR.toString())));
    }

    @Test
    @Transactional
    public void getYears() throws Exception {
        // Initialize the database
        yearsRepository.saveAndFlush(years);

        // Get the years
        restYearsMockMvc.perform(get("/api/years/{id}", years.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(years.getId().intValue()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingYears() throws Exception {
        // Get the years
        restYearsMockMvc.perform(get("/api/years/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateYears() throws Exception {
        // Initialize the database
        yearsRepository.saveAndFlush(years);
        int databaseSizeBeforeUpdate = yearsRepository.findAll().size();

        // Update the years
        Years updatedYears = yearsRepository.findOne(years.getId());
        updatedYears
            .year(UPDATED_YEAR);
        YearsDTO yearsDTO = yearsMapper.toDto(updatedYears);

        restYearsMockMvc.perform(put("/api/years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yearsDTO)))
            .andExpect(status().isOk());

        // Validate the Years in the database
        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeUpdate);
        Years testYears = yearsList.get(yearsList.size() - 1);
        assertThat(testYears.getYear()).isEqualTo(UPDATED_YEAR);
    }

    @Test
    @Transactional
    public void updateNonExistingYears() throws Exception {
        int databaseSizeBeforeUpdate = yearsRepository.findAll().size();

        // Create the Years
        YearsDTO yearsDTO = yearsMapper.toDto(years);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restYearsMockMvc.perform(put("/api/years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yearsDTO)))
            .andExpect(status().isCreated());

        // Validate the Years in the database
        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteYears() throws Exception {
        // Initialize the database
        yearsRepository.saveAndFlush(years);
        int databaseSizeBeforeDelete = yearsRepository.findAll().size();

        // Get the years
        restYearsMockMvc.perform(delete("/api/years/{id}", years.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Years> yearsList = yearsRepository.findAll();
        assertThat(yearsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Years.class);
        Years years1 = new Years();
        years1.setId(1L);
        Years years2 = new Years();
        years2.setId(years1.getId());
        assertThat(years1).isEqualTo(years2);
        years2.setId(2L);
        assertThat(years1).isNotEqualTo(years2);
        years1.setId(null);
        assertThat(years1).isNotEqualTo(years2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(YearsDTO.class);
        YearsDTO yearsDTO1 = new YearsDTO();
        yearsDTO1.setId(1L);
        YearsDTO yearsDTO2 = new YearsDTO();
        assertThat(yearsDTO1).isNotEqualTo(yearsDTO2);
        yearsDTO2.setId(yearsDTO1.getId());
        assertThat(yearsDTO1).isEqualTo(yearsDTO2);
        yearsDTO2.setId(2L);
        assertThat(yearsDTO1).isNotEqualTo(yearsDTO2);
        yearsDTO1.setId(null);
        assertThat(yearsDTO1).isNotEqualTo(yearsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(yearsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(yearsMapper.fromId(null)).isNull();
    }
}
