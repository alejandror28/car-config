package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CartypesFields;
import com.cartienda.carconfig.repository.CartypesFieldsRepository;
import com.cartienda.carconfig.service.CartypesFieldsService;
import com.cartienda.carconfig.service.dto.CartypesFieldsDTO;
import com.cartienda.carconfig.service.mapper.CartypesFieldsMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CartypesFieldsResource REST controller.
 *
 * @see CartypesFieldsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CartypesFieldsResourceIntTest {

    @Autowired
    private CartypesFieldsRepository cartypesFieldsRepository;

    @Autowired
    private CartypesFieldsMapper cartypesFieldsMapper;

    @Autowired
    private CartypesFieldsService cartypesFieldsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCartypesFieldsMockMvc;

    private CartypesFields cartypesFields;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CartypesFieldsResource cartypesFieldsResource = new CartypesFieldsResource(cartypesFieldsService);
        this.restCartypesFieldsMockMvc = MockMvcBuilders.standaloneSetup(cartypesFieldsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CartypesFields createEntity(EntityManager em) {
        CartypesFields cartypesFields = new CartypesFields();
        return cartypesFields;
    }

    @Before
    public void initTest() {
        cartypesFields = createEntity(em);
    }

    @Test
    @Transactional
    public void createCartypesFields() throws Exception {
        int databaseSizeBeforeCreate = cartypesFieldsRepository.findAll().size();

        // Create the CartypesFields
        CartypesFieldsDTO cartypesFieldsDTO = cartypesFieldsMapper.toDto(cartypesFields);
        restCartypesFieldsMockMvc.perform(post("/api/cartypes-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cartypesFieldsDTO)))
            .andExpect(status().isCreated());

        // Validate the CartypesFields in the database
        List<CartypesFields> cartypesFieldsList = cartypesFieldsRepository.findAll();
        assertThat(cartypesFieldsList).hasSize(databaseSizeBeforeCreate + 1);
        CartypesFields testCartypesFields = cartypesFieldsList.get(cartypesFieldsList.size() - 1);
    }

    @Test
    @Transactional
    public void createCartypesFieldsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cartypesFieldsRepository.findAll().size();

        // Create the CartypesFields with an existing ID
        cartypesFields.setId(1L);
        CartypesFieldsDTO cartypesFieldsDTO = cartypesFieldsMapper.toDto(cartypesFields);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCartypesFieldsMockMvc.perform(post("/api/cartypes-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cartypesFieldsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CartypesFields> cartypesFieldsList = cartypesFieldsRepository.findAll();
        assertThat(cartypesFieldsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCartypesFields() throws Exception {
        // Initialize the database
        cartypesFieldsRepository.saveAndFlush(cartypesFields);

        // Get all the cartypesFieldsList
        restCartypesFieldsMockMvc.perform(get("/api/cartypes-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cartypesFields.getId().intValue())));
    }

    @Test
    @Transactional
    public void getCartypesFields() throws Exception {
        // Initialize the database
        cartypesFieldsRepository.saveAndFlush(cartypesFields);

        // Get the cartypesFields
        restCartypesFieldsMockMvc.perform(get("/api/cartypes-fields/{id}", cartypesFields.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cartypesFields.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCartypesFields() throws Exception {
        // Get the cartypesFields
        restCartypesFieldsMockMvc.perform(get("/api/cartypes-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCartypesFields() throws Exception {
        // Initialize the database
        cartypesFieldsRepository.saveAndFlush(cartypesFields);
        int databaseSizeBeforeUpdate = cartypesFieldsRepository.findAll().size();

        // Update the cartypesFields
        CartypesFields updatedCartypesFields = cartypesFieldsRepository.findOne(cartypesFields.getId());
        CartypesFieldsDTO cartypesFieldsDTO = cartypesFieldsMapper.toDto(updatedCartypesFields);

        restCartypesFieldsMockMvc.perform(put("/api/cartypes-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cartypesFieldsDTO)))
            .andExpect(status().isOk());

        // Validate the CartypesFields in the database
        List<CartypesFields> cartypesFieldsList = cartypesFieldsRepository.findAll();
        assertThat(cartypesFieldsList).hasSize(databaseSizeBeforeUpdate);
        CartypesFields testCartypesFields = cartypesFieldsList.get(cartypesFieldsList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCartypesFields() throws Exception {
        int databaseSizeBeforeUpdate = cartypesFieldsRepository.findAll().size();

        // Create the CartypesFields
        CartypesFieldsDTO cartypesFieldsDTO = cartypesFieldsMapper.toDto(cartypesFields);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCartypesFieldsMockMvc.perform(put("/api/cartypes-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cartypesFieldsDTO)))
            .andExpect(status().isCreated());

        // Validate the CartypesFields in the database
        List<CartypesFields> cartypesFieldsList = cartypesFieldsRepository.findAll();
        assertThat(cartypesFieldsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCartypesFields() throws Exception {
        // Initialize the database
        cartypesFieldsRepository.saveAndFlush(cartypesFields);
        int databaseSizeBeforeDelete = cartypesFieldsRepository.findAll().size();

        // Get the cartypesFields
        restCartypesFieldsMockMvc.perform(delete("/api/cartypes-fields/{id}", cartypesFields.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CartypesFields> cartypesFieldsList = cartypesFieldsRepository.findAll();
        assertThat(cartypesFieldsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CartypesFields.class);
        CartypesFields cartypesFields1 = new CartypesFields();
        cartypesFields1.setId(1L);
        CartypesFields cartypesFields2 = new CartypesFields();
        cartypesFields2.setId(cartypesFields1.getId());
        assertThat(cartypesFields1).isEqualTo(cartypesFields2);
        cartypesFields2.setId(2L);
        assertThat(cartypesFields1).isNotEqualTo(cartypesFields2);
        cartypesFields1.setId(null);
        assertThat(cartypesFields1).isNotEqualTo(cartypesFields2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CartypesFieldsDTO.class);
        CartypesFieldsDTO cartypesFieldsDTO1 = new CartypesFieldsDTO();
        cartypesFieldsDTO1.setId(1L);
        CartypesFieldsDTO cartypesFieldsDTO2 = new CartypesFieldsDTO();
        assertThat(cartypesFieldsDTO1).isNotEqualTo(cartypesFieldsDTO2);
        cartypesFieldsDTO2.setId(cartypesFieldsDTO1.getId());
        assertThat(cartypesFieldsDTO1).isEqualTo(cartypesFieldsDTO2);
        cartypesFieldsDTO2.setId(2L);
        assertThat(cartypesFieldsDTO1).isNotEqualTo(cartypesFieldsDTO2);
        cartypesFieldsDTO1.setId(null);
        assertThat(cartypesFieldsDTO1).isNotEqualTo(cartypesFieldsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cartypesFieldsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cartypesFieldsMapper.fromId(null)).isNull();
    }
}
