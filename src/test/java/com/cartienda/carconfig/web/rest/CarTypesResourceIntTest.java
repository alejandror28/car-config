package com.cartienda.carconfig.web.rest;

import com.cartienda.carconfig.CarconfigApp;

import com.cartienda.carconfig.domain.CarTypes;
import com.cartienda.carconfig.repository.CarTypesRepository;
import com.cartienda.carconfig.service.CarTypesService;
import com.cartienda.carconfig.service.dto.CarTypesDTO;
import com.cartienda.carconfig.service.mapper.CarTypesMapper;
import com.cartienda.carconfig.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarTypesResource REST controller.
 *
 * @see CarTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarconfigApp.class)
public class CarTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CarTypesRepository carTypesRepository;

    @Autowired
    private CarTypesMapper carTypesMapper;

    @Autowired
    private CarTypesService carTypesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarTypesMockMvc;

    private CarTypes carTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarTypesResource carTypesResource = new CarTypesResource(carTypesService);
        this.restCarTypesMockMvc = MockMvcBuilders.standaloneSetup(carTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarTypes createEntity(EntityManager em) {
        CarTypes carTypes = new CarTypes()
            .description(DEFAULT_DESCRIPTION);
        return carTypes;
    }

    @Before
    public void initTest() {
        carTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarTypes() throws Exception {
        int databaseSizeBeforeCreate = carTypesRepository.findAll().size();

        // Create the CarTypes
        CarTypesDTO carTypesDTO = carTypesMapper.toDto(carTypes);
        restCarTypesMockMvc.perform(post("/api/car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carTypesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarTypes in the database
        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeCreate + 1);
        CarTypes testCarTypes = carTypesList.get(carTypesList.size() - 1);
        assertThat(testCarTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCarTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carTypesRepository.findAll().size();

        // Create the CarTypes with an existing ID
        carTypes.setId(1L);
        CarTypesDTO carTypesDTO = carTypesMapper.toDto(carTypes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarTypesMockMvc.perform(post("/api/car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carTypesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = carTypesRepository.findAll().size();
        // set the field null
        carTypes.setDescription(null);

        // Create the CarTypes, which fails.
        CarTypesDTO carTypesDTO = carTypesMapper.toDto(carTypes);

        restCarTypesMockMvc.perform(post("/api/car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carTypesDTO)))
            .andExpect(status().isBadRequest());

        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarTypes() throws Exception {
        // Initialize the database
        carTypesRepository.saveAndFlush(carTypes);

        // Get all the carTypesList
        restCarTypesMockMvc.perform(get("/api/car-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getCarTypes() throws Exception {
        // Initialize the database
        carTypesRepository.saveAndFlush(carTypes);

        // Get the carTypes
        restCarTypesMockMvc.perform(get("/api/car-types/{id}", carTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarTypes() throws Exception {
        // Get the carTypes
        restCarTypesMockMvc.perform(get("/api/car-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarTypes() throws Exception {
        // Initialize the database
        carTypesRepository.saveAndFlush(carTypes);
        int databaseSizeBeforeUpdate = carTypesRepository.findAll().size();

        // Update the carTypes
        CarTypes updatedCarTypes = carTypesRepository.findOne(carTypes.getId());
        updatedCarTypes
            .description(UPDATED_DESCRIPTION);
        CarTypesDTO carTypesDTO = carTypesMapper.toDto(updatedCarTypes);

        restCarTypesMockMvc.perform(put("/api/car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carTypesDTO)))
            .andExpect(status().isOk());

        // Validate the CarTypes in the database
        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeUpdate);
        CarTypes testCarTypes = carTypesList.get(carTypesList.size() - 1);
        assertThat(testCarTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCarTypes() throws Exception {
        int databaseSizeBeforeUpdate = carTypesRepository.findAll().size();

        // Create the CarTypes
        CarTypesDTO carTypesDTO = carTypesMapper.toDto(carTypes);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarTypesMockMvc.perform(put("/api/car-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carTypesDTO)))
            .andExpect(status().isCreated());

        // Validate the CarTypes in the database
        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarTypes() throws Exception {
        // Initialize the database
        carTypesRepository.saveAndFlush(carTypes);
        int databaseSizeBeforeDelete = carTypesRepository.findAll().size();

        // Get the carTypes
        restCarTypesMockMvc.perform(delete("/api/car-types/{id}", carTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CarTypes> carTypesList = carTypesRepository.findAll();
        assertThat(carTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarTypes.class);
        CarTypes carTypes1 = new CarTypes();
        carTypes1.setId(1L);
        CarTypes carTypes2 = new CarTypes();
        carTypes2.setId(carTypes1.getId());
        assertThat(carTypes1).isEqualTo(carTypes2);
        carTypes2.setId(2L);
        assertThat(carTypes1).isNotEqualTo(carTypes2);
        carTypes1.setId(null);
        assertThat(carTypes1).isNotEqualTo(carTypes2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarTypesDTO.class);
        CarTypesDTO carTypesDTO1 = new CarTypesDTO();
        carTypesDTO1.setId(1L);
        CarTypesDTO carTypesDTO2 = new CarTypesDTO();
        assertThat(carTypesDTO1).isNotEqualTo(carTypesDTO2);
        carTypesDTO2.setId(carTypesDTO1.getId());
        assertThat(carTypesDTO1).isEqualTo(carTypesDTO2);
        carTypesDTO2.setId(2L);
        assertThat(carTypesDTO1).isNotEqualTo(carTypesDTO2);
        carTypesDTO1.setId(null);
        assertThat(carTypesDTO1).isNotEqualTo(carTypesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(carTypesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(carTypesMapper.fromId(null)).isNull();
    }
}
